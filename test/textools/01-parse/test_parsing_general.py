#!/usr/bin/env python3

# --------------------- #
# -- GENERAL IMPORTS -- #
# --------------------- #

from pathlib import Path
import pytest
import sys


# --------------------------- #
# -- UGLY RELATIVE IMPORTS -- #
# --------------------------- #

THIS_DIR = Path(__file__).parent

DATAS_DIR = THIS_DIR / "general"

QCMONGE_DIR = THIS_DIR

while QCMONGE_DIR.name != "QCMonge":
    QCMONGE_DIR = QCMONGE_DIR.parent

QCMONGE_DIR = QCMONGE_DIR / "qcmonge"

sys.path.append(str(QCMONGE_DIR))  # Z !

from textools.utfize import decode2utf8
from textools.parse import builddatas


# ------------------- #
# -- GOOD COMPILATIONS -- #
# ------------------- #

def test_parse_good_compilation():
    for pathtex in DATAS_DIR.glob('**/*.tex'):
        try:
            utf8string    = decode2utf8(str(pathtex))
            question_list = builddatas(utf8string)

        except Exception as e:
            pytest.fail("one exception raised :\n" + str(e))
