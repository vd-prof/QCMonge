import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "qcmonge"))

from texanalizer.texanalyser import SynTexAnal

if len(sys.argv) > 1:
    fic = open(sys.argv[1], 'r', encoding="utf_8")
    data = fic.read()
    fic.close()
    sta = SynTexAnal(data)
    doc = sta.full_latex_document
    if doc:
        print("\nTout est ok\n")
        print("######################################################")
        print(doc)
        print("######################################################")
        print(doc.rebuild_latex())
        print("######################################################")
        #print(doc.packages)
    else:
        print("\nça a planté quelque part.\n")
        print(sta.analysis_error.error_message());
else:
    print("il manque le nom du fichier LaTeX à analyser...")
