from django.urls import path
from django.contrib.auth import views as auth_views
from qcmonge.views import general, api, exportation, importation
from django.conf import settings

urlpatterns = [
    path('', general.index, name='index'),
    path('user/<str:user_login>', general.voir_user, name='voir-user'),
    path('themes/', general.voir_themes, name='voir-themes'),
        
    path('import/', importation.import_qcm, name='import1-qcm'),
    path('finalise-import/', importation.finalise_import_qcm, name='import2-qcm'),

    path('export/', exportation.export_qcm, name='export-qcm'),
    path('get-qcm-content/', exportation.get_qcm_content, name='get-qcm-content'),

    path('connect/', auth_views.LoginView.as_view(template_name='qcmonge/login.html'), name='login'),
    path('disconnect/', auth_views.LogoutView.as_view(template_name='qcmonge/login.html'), name='logout'),
    
    path('passwd/change/', auth_views.PasswordChangeView.as_view(template_name='qcmonge/change-pwd-do.html'), name='password_change'),
    path('passwd/change-done/', auth_views.PasswordChangeDoneView.as_view(template_name='qcmonge/change-pwd-done.html'), name='password_change_done'),
    path('passwd/reset/', auth_views.PasswordResetView.as_view(template_name='qcmonge/reset-pwd-ask.html', extra_email_context={'site_name':settings.QCMONGE_SITE_NAME, 'domain':settings.QCMONGE_DOMAIN}), name='password_reset'),
    path('passwd/reset-sent/', auth_views.PasswordResetDoneView.as_view(template_name='qcmonge/reset-pwd-sent.html'), name='password_reset_done'),
    path('passwd/reset-do/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='qcmonge/reset-pwd-do.html'), name='password_reset_confirm'),
    path('passwd/reset-done/', auth_views.PasswordResetCompleteView.as_view(template_name='qcmonge/reset-pwd-done.html'), name='password_reset_complete'),

    path('api/structure/', api.api_structure, name='api-structure'),
    path('api/search-questions/', api.api_search_questions, name='api-search-questions'),
    path('api/infos-questions/', api.api_infos_questions, name='api-infos-questions'),
]
