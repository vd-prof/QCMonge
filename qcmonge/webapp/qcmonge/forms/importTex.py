from django import forms
from django.core.validators import FileExtensionValidator
from django.core.exceptions import ValidationError
from django.conf import settings

from qcmonge.forms.thematique import ThematiqueForm
from qcmonge.models.question import Question

from texanalizer.texutils import compile_question, extract_questions
from texanalizer.texanalyseconstants import TexAnalyseConstants as TAC

import magic
import chardet
import os






class ImportTexForm(ThematiqueForm):

    texupload = forms.FileField(label="Sélectionner le fichier TeX de votre QCM",
                                validators=[FileExtensionValidator(allowed_extensions=['tex'])])


    
    def __init__(self, auteur, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.nb_questions_non_valides = 0
        self.auteur = auteur
        


    def clean_texupload(self):
        ficontent = self.cleaned_data['texupload']
        content = ficontent.read()
        mime = magic.from_buffer(content, mime=True)
        if not 'tex' in mime:
            raise forms.ValidationError("Le fichier joint n'a pas le bon mime/type (" + mime + ").")
        infosfound = chardet.detect(content)
        encoding = infosfound['encoding']
        # External "BUG" : chardet fails to find latin-1 ! It can find latin-2 instead.
        # L'outils de CB faisait appel à bash... 
        content = content.decode(encoding, errors = "backslashreplace")
        return content

    
    def clean(self):
        cleaned = self.cleaned_data
        try:
            content = cleaned['texupload']
            questions = extract_questions(content, TAC.EXCLUDED_PACKAGES)
            nbok = 0
            for quest in questions:
                tex_header = quest["tex_header"]
                content = quest["enonce"]
                tmpimgfic = os.path.join(settings.STATIC_TEMP_IMAGE_DIR, "tmp-" + str(self.auteur.id) + "-" + str(nbok) + ".png")
                if compile_question(tex_header, content, tmpimgfic):
                    obj = Question.objects.create()
                    obj.qtype = quest['type']
                    obj.enonce = content.strip(" \t\n").replace("\r", "")
                    obj.tex_header = tex_header.strip(" \t\n").replace("\r", "")
                    obj.auteur = self.auteur
                    obj.valide = False
                    if cleaned['soustheme']:
                        obj.thematique = cleaned['soustheme']
                    if quest["soustheme"]:
                        try:
                            obj.thematique = SousTheme.objects.get( motcle=thmname )
                        except:
                            pass
                    obj.normalise_image_name(tmpimgfic)
                    obj.save()
                    nbok += 1
            self.nb_questions_non_valides = len(questions) - nbok
        except Exception as e:
            raise ValidationError("Problème de détection de questions: " + str(e))
        return self.cleaned_data
