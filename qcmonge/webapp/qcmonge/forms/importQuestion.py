from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError

from qcmonge.models.question import Question

from qcmonge.forms.thematique import ThematiqueForm

from texanalizer.texutils import compile_question

import os
from random import random



class QuestionImportForm(ThematiqueForm):

    NOTHING_DONE = 0
    DELETED_QUESTION = -1
    UPDATED_QUESTION = 1
    CREATED_QUESTION = 2
    
    
    def __init__(self, question, auteur, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.question = question
        self.auteur = auteur
        self.done = QuestionImportForm.NOTHING_DONE

        
    qtype = forms.ChoiceField(label="Type de question", choices=list(Question.QUESTION_TYPE))

    keepme = forms.BooleanField(label="Inclure cette question dans la base", required=False)
    
    texheader = forms.CharField(label="préambule LaTeX", widget=forms.Textarea(attrs={"rows":6, "cols":60}), required=False)
    texcontent = forms.CharField(label="version LaTeX", widget=forms.Textarea(attrs={"rows":18, "cols":60}))


    def clean_texheader(self):
        data = self.cleaned_data['texheader']
        return data.strip(" \t\n").replace("\r", "")

    def clean_texcontent(self):
        data = self.cleaned_data['texcontent']
        return data.strip(" \t\n").replace("\r", "")
    
    def clean_soustheme(self):
        data = self.cleaned_data['soustheme']
        if not data:
            raise ValidationError("Sous-Thème non précisé")
        return data


    def clean(self):
        cleaned = self.cleaned_data
        if not cleaned['keepme']:
            if self.question:
                try:
                    os.remove(os.path.join(settings.STATIC_FINAL_IMAGE_DIR, self.question.fichier_image))
                except:
                    pass
                self.question.delete()
                self.done = QuestionImportForm.DELETED_QUESTION

        else:
            tex_header = cleaned['texheader']
            content = cleaned['texcontent']
            compilation = True
            fullpath_image = None
            if not(self.question != None and self.question.tex_header == tex_header and self.question.enonce == content):
                imgfic = "tmp-" + str(self.auteur.id) + "-" + str(random()) + ".png"
                fullpath_image = os.path.join(settings.STATIC_TEMP_IMAGE_DIR, imgfic)
                compilation = compile_question(tex_header, content, fullpath_image)
            if compilation:
                if self.question == None:
                    self.question = Question.objects.create()
                    self.done = QuestionImportForm.CREATED_QUESTION
                else:
                    self.done = QuestionImportForm.UPDATED_QUESTION
                self.question.qtype = cleaned['qtype']
                self.question.enonce = cleaned['texcontent']
                self.question.tex_header = cleaned['texheader']
                self.question.normalise_image_name(fullpath_image)
                self.question.auteur = self.auteur
                self.question.thematique = cleaned['soustheme']
                self.question.valide = True
                self.question.save()
            else:
                raise ValidationError("La question ne compile pas. Vérifier la cohérence de vos données LaTeX.")
        return cleaned
            
