from django.forms import Form, ChoiceField
from django.core.exceptions import ValidationError

from qcmonge.models.classement import Matiere, Theme, SousTheme


NOTHING_SELECTED = '---'


class MatiereChoiceField(ChoiceField):
    def to_python(self, value):
        matiere = None
        if value and value != NOTHING_SELECTED:
            try:
                matiere = Matiere.objects.get(intitule=value)
            except:
                raise ValidationError("Matière inconnue")
        return matiere

    def validate(self, value):
        pass
    



class ThemeChoiceField(ChoiceField):
    def to_python(self, value):
        theme = None
        if value and value != NOTHING_SELECTED:
            try:
                theme = Theme.objects.get(sujet=value)
            except:
                raise ValidationError("Thème inconnue")
        return theme
    
    def validate(self, value):
        pass
    



class SousthemeChoiceField(ChoiceField):
    def to_python(self, value):
        soustheme = None
        if value and value != NOTHING_SELECTED:
            try:
                soustheme = SousTheme.objects.get(motcle=value)
            except:
                raise ValidationError("Sous-Thème inconnue")
        return soustheme
    
    def validate(self, value):
        pass
    






class ThematiqueForm(Form):

    matiere = MatiereChoiceField(label="Matière", required=False)
    theme = ThemeChoiceField(label="Thème", required=False)
    soustheme = SousthemeChoiceField(label="Sous-thème", required=False)


    def clean(self):
        cleaned = super().clean()
        matiere = cleaned.get('matiere')
        if matiere:
            theme = cleaned.get('theme')
            if theme:
                if theme.domaine != matiere:
                    raise ValidationError("Thème incompatible")
                soustheme = cleaned.get('soustheme')
                if soustheme:
                    if soustheme.theme != theme:
                        raise ValidationError("Sous-Thème incompatible")
            else:
                cleaned['soustheme'] = None
        else:
            cleaned['theme'] = None
            cleaned['soustheme'] = None
        return cleaned


    
