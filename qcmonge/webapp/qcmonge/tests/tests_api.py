from unittest import expectedFailure

from django.test import TestCase
from django.urls import reverse

from django.contrib.auth.models import User

from qcmonge.models.question import Question
from qcmonge.models.classement import Matiere, Theme, SousTheme

import json


class ApiTestCase(TestCase):
    def setUp(self):
        self.passwd = 'top_secret'
        self.user1 = User.objects.create_user(username='meeple1', password=self.passwd)
        self.user2 = User.objects.create_user(username='meeple2', password=self.passwd)
        
        self.matiere1 = Matiere.objects.create(intitule="matière1")
        self.matiere2 = Matiere.objects.create(intitule="m2")
        self.matiere3 = Matiere.objects.create(intitule="m3")

        self.theme1 = Theme.objects.create(sujet="thème1", domaine=self.matiere2)
        self.theme2 = Theme.objects.create(sujet="t2", domaine=self.matiere2)
        self.theme3 = Theme.objects.create(sujet="t3", domaine=self.matiere2)
        self.theme4 = Theme.objects.create(sujet="t4", domaine=self.matiere3)

        self.soustheme1 = SousTheme.objects.create(motcle="sous-thème1", theme=self.theme2)
        self.soustheme2 = SousTheme.objects.create(motcle="s2", theme=self.theme3)
        self.soustheme3 = SousTheme.objects.create(motcle="s3", theme=self.theme4)
        self.soustheme4 = SousTheme.objects.create(motcle="s4", theme=self.theme3)

        self.question01 = Question.objects.create(qtype='S', enonce="blabla01", tex_header="", auteur=self.user1, thematique=self.soustheme2, valide=False)
        self.question02 = Question.objects.create(qtype='S', enonce="blabla02", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question03 = Question.objects.create(qtype='S', enonce="blabla03", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question04 = Question.objects.create(qtype='S', enonce="blabla04", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question05 = Question.objects.create(qtype='S', enonce="blabla05", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question06 = Question.objects.create(qtype='S', enonce="blabla06", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question07 = Question.objects.create(qtype='M', enonce="blabla07", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question08 = Question.objects.create(qtype='M', enonce="blabla08", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question09 = Question.objects.create(qtype='O', enonce="blabla09", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=True)
        self.question10 = Question.objects.create(qtype='S', enonce="blabla10", tex_header="", auteur=self.user1, thematique=self.soustheme3, valide=False)
        self.question11 = Question.objects.create(qtype='S', enonce="blabla11", tex_header="", auteur=self.user2, thematique=self.soustheme3, valide=False)
        self.question12 = Question.objects.create(qtype='M', enonce="blabla12", tex_header="", auteur=self.user1, thematique=self.soustheme4, valide=False)
        """
        Structure:
        matiere1:
        matiere2:
            theme1:
            theme2:
                soustheme1:
            theme3:
                soustheme2:
                    question01 (S, nv)
                    question11 (S, nv, user2)
                soustheme4:
                    question12 (M, nv)
        matiere3:
            theme4:
                soustheme3:
                    question02 (S, V)
                    question03 (S, V)
                    question04 (S, V)
                    question05 (S, V)
                    question06 (S, V)
                    question07 (M, V)
                    question08 (M, V)
                    question09 (O, V)
                    question10 (S, nv)
        """


    def do_json_post_test(self, url, json_input, json_awaited, error_message):
        response = self.client.post(url, {'jsondata': json.dumps(json_input)})
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu en POST ici")
        received_content = response.content.decode("utf-8")
        self.assertJSONEqual(received_content, json_awaited, error_message)

    
    def test_get_structure(self):
        url = reverse('api-structure')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, "Code HTML 404 attendu: pas de GET ici")

    def test_post_structure_nonempty_only_even_non_validated(self):
        url = reverse('api-structure')
        json_input = {"nonempty_only": True, "even_non_validated": True}
        json_output = {'success': True,
                       'data': {self.matiere2.intitule: {self.theme3.sujet: [self.soustheme2.motcle, self.soustheme4.motcle]},
                                self.matiere3.intitule: {self.theme4.sujet: [self.soustheme3.motcle]}}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    def test_post_structure_nonempty_only_validated_only(self):
        url = reverse('api-structure')
        json_input = {"nonempty_only": True, "even_non_validated": False}
        json_output = {'success': True,
                       'data': {self.matiere3.intitule: {self.theme4.sujet: [self.soustheme3.motcle]}}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")
        
    def test_post_structure_everything(self):
        url = reverse('api-structure')
        json_input = {"nonempty_only": False, "even_non_validated": True}
        json_output = {'success': True,
                       'data': {self.matiere1.intitule: {},
                                self.matiere2.intitule: {self.theme1.sujet:[],
                                                         self.theme2.sujet: [self.soustheme1.motcle],
                                                         self.theme3.sujet: [self.soustheme2.motcle, self.soustheme4.motcle]},
                                self.matiere3.intitule: {self.theme4.sujet:[self.soustheme3.motcle]}
                       }}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    
    def test_get_search(self):
        url = reverse('api-search-questions')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, "Code HTML 404 attendu: pas de GET ici")

    def test_post_search_error_1(self):
        url = reverse('api-search-questions')
        # pas de tag matière
        json_input = {}
        response = self.client.post(url, {'jsondata': json.dumps(json_input)})
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: du POST ici")
        jresp = json.loads(response.content)
        self.assertFalse(jresp['success'], "manque la matière")

    def test_post_search_error_2(self):
        url = reverse('api-search-questions')
        # pas de tag thème
        json_input = {"matiere": self.matiere1.intitule}
        response = self.client.post(url, {'jsondata': json.dumps(json_input)})
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: du POST ici")
        jresp = json.loads(response.content)
        self.assertFalse(jresp['success'], "manque le theme")

    def test_post_search_error_3(self):
        url = reverse('api-search-questions')
        # pas de tag sous-thème
        json_input = {"matiere": self.matiere2.intitule, "theme": self.theme1.sujet }
        response = self.client.post(url, {'jsondata': json.dumps(json_input)})
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: du POST ici")
        jresp = json.loads(response.content)
        self.assertFalse(jresp['success'], "manque le sous-theme")

    def test_post_search_error_4(self):
        url = reverse('api-search-questions')
        # pas de tag type de question
        json_input = {"matiere": self.matiere2.intitule, "theme": self.theme3.sujet, "soustheme": self.soustheme4.motcle}
        response = self.client.post(url, {'jsondata': json.dumps(json_input)})
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: du POST ici")
        jresp = json.loads(response.content)
        self.assertFalse(jresp['success'], "manque le type")

        
    def test_post_search_ok_1(self):
        url = reverse('api-search-questions')
        # pas de question
        json_input = {"matiere": self.matiere2.intitule, "theme": self.theme3.sujet, "soustheme": self.soustheme4.motcle, "type": "SMO"}
        json_output = {'success': True, "data": {"questions": [], "offset": 0, "total": 0}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    def test_post_search_ok_2(self):
        url = reverse('api-search-questions')
        # une seule question parmi plusieurs
        json_input = {"matiere": self.matiere3.intitule, "theme": self.theme4.sujet, "soustheme": self.soustheme3.motcle, "type": "O"}
        json_output = {'success': True, "data": {"offset": 0, "total": 1,
                                                 "questions": [{"id": self.question09.qid, "enonce": self.question09.enonce, "image": "/static/"+self.question09.fichier_image}]}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    def test_post_search_ok_3(self):
        url = reverse('api-search-questions')
        # plusieurs questions
        json_input = {"matiere": self.matiere3.intitule, "theme": self.theme4.sujet, "soustheme": self.soustheme3.motcle, "type": "M"}
        json_output = {'success': True, "data": {"offset": 0, "total": 2,
                                                 "questions": [
                                                     {"id": self.question07.qid, "enonce": self.question07.enonce, "image": "/static/"+self.question07.fichier_image},
                                                     {"id": self.question08.qid, "enonce": self.question08.enonce, "image": "/static/"+self.question08.fichier_image},
                                                 ]}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    def test_post_search_ok_4(self):
        url = reverse('api-search-questions')
        # plusieurs questions mais pas toutes
        json_input = {"matiere": self.matiere3.intitule, "theme": self.theme4.sujet, "soustheme": self.soustheme3.motcle, "type": "S", "qlength": 4}
        json_output = {'success': True, "data": {"offset": 0, "total": 5,
                                                 "questions": [
                                                     {"id": self.question02.qid, "enonce": self.question02.enonce, "image": "/static/"+self.question02.fichier_image},
                                                     {"id": self.question03.qid, "enonce": self.question03.enonce, "image": "/static/"+self.question03.fichier_image},
                                                     {"id": self.question04.qid, "enonce": self.question04.enonce, "image": "/static/"+self.question04.fichier_image},
                                                     {"id": self.question05.qid, "enonce": self.question05.enonce, "image": "/static/"+self.question05.fichier_image},
                                                 ]}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    def test_post_search_ok_5(self):
        url = reverse('api-search-questions')
        # deuxième page de réponses
        json_input = {"matiere": self.matiere3.intitule, "theme": self.theme4.sujet, "soustheme": self.soustheme3.motcle, "type": "S", "qoffset": 1, "qlength": 2}
        json_output = {'success': True, "data": {"offset": 1, "total": 5,
                                                 "questions": [
                                                     {"id": self.question03.qid, "enonce": self.question03.enonce, "image": "/static/"+self.question03.fichier_image},
                                                     {"id": self.question04.qid, "enonce": self.question04.enonce, "image": "/static/"+self.question04.fichier_image},
                                                 ]}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")

    def test_post_search_ok_6(self):
        url = reverse('api-search-questions')
        # plus loin que la dernière page
        json_input = {"matiere": self.matiere3.intitule, "theme": self.theme4.sujet, "soustheme": self.soustheme3.motcle, "type": "SMO", "qoffset": 100, "qlength": 2}
        json_output = {'success': True, "data": {"offset": 0, "total": 8,
                                                 "questions": [
                                                     {"id": self.question02.qid, "enonce": self.question02.enonce, "image": "/static/"+self.question02.fichier_image},
                                                     {"id": self.question03.qid, "enonce": self.question03.enonce, "image": "/static/"+self.question03.fichier_image},
                                                 ]}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")
        

    
    def test_get_infos(self):
        url = reverse('api-infos-questions')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, "Code HTML 404 attendu: pas de GET ici")

    def test_post_infos_error(self):
        url = reverse('api-infos-questions')
        json_input = {"questions": [1000]}
        response = self.client.post(url, {'jsondata': json.dumps(json_input)})
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: du POST ici")
        jresp = json.loads(response.content)
        self.assertFalse(jresp['success'], "question inexistante")

    def test_post_infos_correct_valide(self):
        url = reverse('api-infos-questions')
        json_input = {"questions": [2,6]}
        json_output = {'success': True,
                       'data': {"2": {"img": "/static/" + self.question02.fichier_image, "enonce": self.question02.enonce},
                                "6": {"img": "/static/" + self.question06.fichier_image, "enonce": self.question06.enonce}}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")


    def test_post_infos_correct_nonvalide(self):
        url = reverse('api-infos-questions')
        json_input = {"questions": [1]}
        json_output = {'success': True,
                       'data': {"1": {"img": "/static/" + self.question01.fichier_image, "enonce": self.question01.enonce}}}
        self.do_json_post_test(url, json_input, json_output, "le json renvoyé n'est pas le bon")
