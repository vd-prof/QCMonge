from unittest import expectedFailure

from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile

from django.contrib.auth.models import User
from qcmonge.models.classement import Matiere, Theme, SousTheme
from qcmonge.models.question import Question

from qcmonge.forms.importQuestion import QuestionImportForm
from qcmonge.forms.importTex import ImportTexForm
from qcmonge.forms.thematique import ThematiqueForm



class ThematiqueTestCase(TestCase):
    def setUp(self):
        self.matiere1 = Matiere.objects.create(intitule="Matière Un")
        self.theme1 = Theme.objects.create(sujet="Thème Un", domaine=self.matiere1)
        self.soustheme1 = SousTheme.objects.create(motcle="Sous-Thème un", theme=self.theme1)

        self.matiere2 = Matiere.objects.create(intitule="Matière Deux")
        self.theme2 = Theme.objects.create(sujet="Thème Deux", domaine=self.matiere2)
        self.soustheme2 = SousTheme.objects.create(motcle="Sous-Thème Deux", theme=self.theme2)

        
    def test_matiere_invalide(self):
        data = {"matiere": "fake"}
        form = ThematiqueForm(data)
        self.assertFalse(form.is_valid(), "formulaire invalide")
        errors = dict(form.errors)
        self.assertTrue("matiere" in errors, "Matière invalide")

        
    def test_theme_invalide_1(self):
        data = {"matiere": self.matiere1.intitule, "theme": "fake"}
        form = ThematiqueForm(data)
        self.assertFalse(form.is_valid(), "formulaire invalide")
        errors = dict(form.errors)
        self.assertTrue("theme" in errors, "Thème invalide")

    def test_theme_invalide_2(self):
        data = {"matiere": self.matiere1.intitule, "theme": self.theme2.sujet}
        form = ThematiqueForm(data)
        self.assertFalse(form.is_valid(), "formulaire invalide")
        errors = dict(form.errors)
        self.assertIn("Thème incompatible", errors["__all__"], "Thème ne correspond pas")

        
    def test_soustheme_invalide_1(self):
        data = {"matiere": self.matiere1.intitule, "theme": self.theme1.sujet, "soustheme": "fake"}
        form = ThematiqueForm(data)
        self.assertFalse(form.is_valid(), "formulaire invalide")
        errors = dict(form.errors)
        self.assertTrue("soustheme" in errors, "sous-Thème invalide")

    def test_soustheme_invalide_2(self):
        data = {"matiere": self.matiere1.intitule, "theme": self.theme1.sujet, "soustheme": self.soustheme2.motcle}
        form = ThematiqueForm(data)
        self.assertFalse(form.is_valid(), "formulaire invalide")
        errors = dict(form.errors)
        self.assertIn("Sous-Thème incompatible", errors["__all__"], "sous-Thème ne correspond pas")

        
    def test_matiere_vide(self):
        data = {"matiere": "---"}
        form = ThematiqueForm(data)
        self.assertTrue(form.is_valid(), "formulaire valide")
        self.assertEqual(form.cleaned_data['matiere'], None, "matière vide")
        self.assertEqual(form.cleaned_data['theme'], None, "thème vide")
        self.assertEqual(form.cleaned_data['soustheme'], None, "sousthème vide")

    def test_matiere_vide_avec_autres(self):
        data = {"matiere": "---", "theme": self.theme1.sujet, "soustheme": self.soustheme1.motcle}
        form = ThematiqueForm(data)
        self.assertTrue(form.is_valid(), "formulaire valide")
        self.assertEqual(form.cleaned_data['matiere'], None, "matière vide")
        self.assertEqual(form.cleaned_data['theme'], None, "thème vide")
        self.assertEqual(form.cleaned_data['soustheme'], None, "sousthème vide")

        
    def test_theme_vide_1(self):
        data = {"matiere": self.matiere1.intitule, "theme": "---"}
        form = ThematiqueForm(data)
        self.assertTrue(form.is_valid(), "formulaire valide")
        self.assertEqual(form.cleaned_data['matiere'], self.matiere1, "matière correcte")
        self.assertEqual(form.cleaned_data['theme'], None, "thème vide")
        self.assertEqual(form.cleaned_data['soustheme'], None, "sousthème vide")

    def test_theme_vide_2(self):
        data = {"matiere": self.matiere1.intitule, "theme": "---", "soustheme": self.soustheme1.motcle}
        form = ThematiqueForm(data)
        self.assertTrue(form.is_valid(), "formulaire valide")
        self.assertEqual(form.cleaned_data['matiere'], self.matiere1, "matière correcte")
        self.assertEqual(form.cleaned_data['theme'], None, "thème vide")
        self.assertEqual(form.cleaned_data['soustheme'], None, "sousthème vide")

        
    def test_soustheme_vide(self):
        data = {"matiere": self.matiere1.intitule, "theme": self.theme1.sujet, "soustheme": "---"}
        form = ThematiqueForm(data)
        self.assertTrue(form.is_valid(), "formulaire valide")
        self.assertEqual(form.cleaned_data['matiere'], self.matiere1, "matière correcte")
        self.assertEqual(form.cleaned_data['theme'], self.theme1, "thème correct")
        self.assertEqual(form.cleaned_data['soustheme'], None, "sousthème vide")


    def test_donnees_valides(self):
        data = {"matiere": self.matiere1.intitule, "theme": self.theme1.sujet, "soustheme": self.soustheme1.motcle}
        form = ThematiqueForm(data)
        self.assertTrue(form.is_valid(), "formulaire valide")
        self.assertEqual(form.cleaned_data['matiere'], self.matiere1, "matière correcte")
        self.assertEqual(form.cleaned_data['theme'], self.theme1, "thème correct")
        self.assertEqual(form.cleaned_data['soustheme'], self.soustheme1, "sousthème correct")


    def test_donnees_vides(self):
        form = ThematiqueForm({})
        self.assertTrue(form.is_valid(), "formulaire vide valide")
        self.assertEqual(form.cleaned_data['matiere'], None, "matière correcte")
        self.assertEqual(form.cleaned_data['theme'], None, "thème correct")
        self.assertEqual(form.cleaned_data['soustheme'], None, "sousthème correct")

        




class ImportQuestionTestCase(TestCase):
    def setUp(self):
        self.matiere = Matiere.objects.create(intitule="Matière Un")
        self.theme = Theme.objects.create(sujet="Thème Un", domaine=self.matiere)
        self.soustheme = SousTheme.objects.create(motcle="Sous-Thème un", theme=self.theme)

        self.passwd = 'top_secret'
        self.admin = User.objects.create_user(username='god', password=self.passwd, is_superuser=True)

        self.question1 = Question.objects.create(qtype='S', enonce="blabla", auteur = self.admin, valide=False)
        self.question2 = Question.objects.create(qtype='S', enonce="blabla", auteur = self.admin, valide=False)


    def test_no_texcontent(self):
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S"}
        form = QuestionImportForm(self.question1, self.admin, data)
        self.assertFalse(form.is_valid(), "pas de texcontent")

    def test_no_qtype(self):
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "texcontent": "blablabla"}
        form = QuestionImportForm(self.question1, self.admin, data)
        self.assertFalse(form.is_valid(), "pas de qtype")

    def test_no_soustheme(self):
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "texcontent": "blablabla", "qtype": "S"}
        form = QuestionImportForm(self.question1, self.admin, data)
        self.assertFalse(form.is_valid(), "pas de soustheme")

    def test_tex_invalides(self):
        content = "\blabla"
        header = ""
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "texcontent": content, "texheader": header, "qtype": "S", "keepme": True}
        form = QuestionImportForm(self.question1, self.admin, data)
        self.assertFalse(form.is_valid(), "données tex invalides")

    def test_tex_valides_garde(self):
        nbquest = len(Question.objects.all())
        content = "blabla"
        header = ""
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "texcontent": content, "texheader": header, "qtype": "S", "keepme": True}
        form = QuestionImportForm(self.question1, self.admin, data)
        self.assertTrue(form.is_valid(), "données tex valides")
        self.assertEqual(len(Question.objects.all()), nbquest, "question gardée")
        self.assertTrue(self.question1.valide, "question validée")

    def test_tex_valides_remove(self):
        nbquest = len(Question.objects.all())
        content = "blabla"
        header = ""
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "texcontent": content, "texheader": header, "qtype": "S"}
        form = QuestionImportForm(self.question2, self.admin, data)
        self.assertTrue(form.is_valid(), "données tex valides")
        self.assertEqual(len(Question.objects.all()), nbquest-1, "question effacée")

    def test_tex_valides_nouveau(self):
        nbquest = len(Question.objects.all())
        content = "blabla"
        header = ""
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "texcontent": content, "texheader": header, "qtype": "S", "keepme": True}
        form = QuestionImportForm(None, self.admin, data)
        self.assertTrue(form.is_valid(), "données tex valides")
        self.assertEqual(len(Question.objects.all()), nbquest+1, "question créée")
        
    def test_trim_latex(self):
        tex_in = "  blablabla\n"
        tex_out = "blablabla"
        head_in = "\t\\usepackage{eurosym}\n  "
        head_out = "\\usepackage{eurosym}"
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "texcontent": tex_in, "qtype": "S", "texheader": head_in, "keepme": True}
        form = QuestionImportForm(self.question2, self.admin, data)
        self.assertTrue(form.is_valid(), "données valides")
        self.assertEqual(self.question2.enonce, tex_out, "texcontent trim")
        self.assertEqual(self.question2.tex_header, head_out, "texheader trim")
        






        
class ImportTexTestCase(TestCase):
    def setUp(self):
        self.matiere = Matiere.objects.create(intitule="Matière Un")
        self.theme = Theme.objects.create(sujet="Thème Un", domaine=self.matiere)
        self.soustheme = SousTheme.objects.create(motcle="Sous-Thème un", theme=self.theme)
        
        self.passwd = 'top_secret'
        self.admin = User.objects.create_user(username='god', password=self.passwd, is_superuser=True)
    
    def test_no_file(self):
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S"}
        form = ImportTexForm(self.admin, data)
        self.assertFalse(form.is_valid(), "pas de fichier")


    def test_file_bad_extension(self):
        file_content = "blabla"
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S"}
        file_data = {"texupload": SimpleUploadedFile("toto.org", file_content.encode("utf-8"))}
        form = ImportTexForm(self.admin, data, file_data)
        self.assertFalse(form.is_valid(), "extension de fichier incorrecte")


    def test_file_donnees_valides_utf8(self):
        nbquest = len(Question.objects.all())
        file_content = "\\documentclass{article}\n\\usepackage{eurosym}\n\\begin{document}\n\\begin{question}Matières, Thêmes, sous-Thèmes\\end{question}\n\\end{document}\n"
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S"}
        file_data = {"texupload": SimpleUploadedFile("toto.tex", file_content.encode("utf-8"))}
        form = ImportTexForm(self.admin, data, file_data)
        self.assertTrue(form.is_valid(), "données valides")
        self.assertEqual(form.cleaned_data['texupload'], file_content, "encodage fichier ok")
        self.assertEqual(len(Question.objects.all()), nbquest+1, "question ajoutée")

        
    def test_file_donnees_valides_iso8859(self):
        nbquest = len(Question.objects.all())
        file_content = "\\documentclass{article}\n\\usepackage{eurosym}\n\\begin{document}\n\\begin{question}{32}Matières, Thêmes, sous-Thèmes\\end{question}\n\\begin{question}{22}Deuxième question\\end{question}\n\\end{document}\n"
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S"}
        file_data = {"texupload": SimpleUploadedFile("toto.tex", file_content.encode("iso-8859-1"))}
        form = ImportTexForm(self.admin, data, file_data)
        self.assertTrue(form.is_valid(), "données valides")
        self.assertEqual(form.cleaned_data['texupload'], file_content, "encodage fichier ok")
        self.assertEqual(len(Question.objects.all()), nbquest+2, "questions ajoutées")

        
    def test_file_donnees_valides_latin1(self):
        nbquest = len(Question.objects.all())
        file_content = "\\documentclass{article}\n\\usepackage{eurosym}\n\\begin{document}\n\\begin{question}{32}Matières, Thêmes, sous-Thèmes\\end{question}\n\\begin{question}{22}Deuxième question\\end{question}\n\\end{document}\n"
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S"}
        file_data = {"texupload": SimpleUploadedFile("toto.tex", file_content.encode("latin-1"))}
        form = ImportTexForm(self.admin, data, file_data)
        self.assertTrue(form.is_valid(), "données valides")
        self.assertEqual(form.cleaned_data['texupload'], file_content, "encodage fichier ok")
        self.assertEqual(len(Question.objects.all()), nbquest+2, "questions ajoutées")
