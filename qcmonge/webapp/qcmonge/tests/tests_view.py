from unittest import expectedFailure

from django.test import TestCase
from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User, Permission

from qcmonge.models.question import Question
from qcmonge.models.classement import Matiere, Theme, SousTheme


"""
Classe pour tester le comportement des pages générales.
 * index
 * user
 * themes
"""
class GeneralTestCase(TestCase):
    def setUp(self):
        self.passwd = 'top_secret'
        self.user = User.objects.create_user(username='john', password=self.passwd)


    def test_index_view(self):
        url = reverse('index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu")
        self.assertTemplateUsed(response, 'qcmonge/index.html', "Le template html n'est pas le bon")

        
    def test_user_view_with_nobody(self):
        nobody = 'unknown'
        url = reverse('voir-user', kwargs={'user_login': nobody})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, "utilisateur non existant")

    def test_user_view_with_someone(self):
        url = reverse('voir-user', kwargs={'user_login': self.user.username})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu")
        self.assertTemplateUsed(response, 'qcmonge/user.html', "Le template html n'est pas le bon")

        
    def test_themes_view(self):
        url = reverse('voir-themes')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu")
        self.assertTemplateUsed(response, 'qcmonge/voir_themes.html', "Le template html n'est pas le bon")





"""
Classe pour tester le comportement des pages d'export.
 * export
 * get-qcm-content
"""
class ExportTestCase(TestCase):
    def setUp(self):
        pass

    def test_export_view(self):
        url = reverse('export-qcm')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu")
        self.assertTemplateUsed(response, 'qcmonge/export_qcm.html', "Le template html n'est pas le bon")

    @expectedFailure
    def test_export_view_matiere(self):
        # tester si la matiere preferée est bien la bonne pour un user anonyme, un user sans mat pref et un user avec mat pref.
        self.fail("test non écrit")

    def test_get_qcm_view(self):
        url = reverse('get-qcm-content')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404, "Code HTML 404 attendu: pas de GET ici")

    @expectedFailure
    def test_post_qcm_view(self):
        #response = self.client.post(url)
        #self.assertEqual(response.status_code, 200, "Code HTML 404 attendu")
        #self.assertTemplateUsed(response, 'qcmonge/export_qcm.html', "Le template html n'est pas le bon")
        self.fail("test non écrit")






"""
Classe pour tester le comportement des pages d'import.
 * import
 * finalise-import
"""
class ImportTestCase(TestCase):
    def setUp(self):
        self.passwd = 'top_secret'
        self.user = User.objects.create_user(username='meeple', password=self.passwd)
        self.redac = User.objects.create_user(username='hero', password=self.passwd)
        content_type = ContentType.objects.get_for_model(Question)
        permission = Permission.objects.get(codename='add_question', content_type=content_type)
        self.redac.user_permissions.add(permission)
        self.redac = User.objects.get(username='hero')
        self.admin = User.objects.create_user(username='god', password=self.passwd, is_superuser=True)

        self.matiere = Matiere.objects.create(intitule="Matière Un")
        self.theme = Theme.objects.create(sujet="Thème Un", domaine=self.matiere)
        self.soustheme = SousTheme.objects.create(motcle="Sous-Thème un", theme=self.theme)
        


    def test_get_import_view_with_no_user(self):
        url = reverse('import1-qcm')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302, "Code HTML 302 attendu: utilisateur non identifié")
        self.assertRedirects(response, reverse('login') + "?next=" + url)

    def test_get_import_view_with_no_permission_user(self):
        url = reverse('import1-qcm')
        self.client.login(username=self.user.username, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302, "Code HTML 302 attendu: utilisateur identifié sans les droits")
        self.assertRedirects(response, reverse('login') + "?next=" + url)
        self.client.logout()

    def test_get_import_view_with_permission_user(self):
        url = reverse('import1-qcm')
        self.client.login(username=self.redac.username, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: utilisateur identifié avec les droits")
        self.assertTemplateUsed(response, 'qcmonge/import1_qcm.html', "Le template html n'est pas le bon")
        self.client.logout()

    def test_get_import_view_with_superuser(self):
        url = reverse('import1-qcm')
        self.client.login(username=self.admin.username, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: admin")
        self.assertTemplateUsed(response, 'qcmonge/import1_qcm.html', "Le template html n'est pas le bon")
        self.client.logout()

    def test_post_import_view_with_no_user(self):
        url = reverse('import1-qcm')
        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302, "Code HTML 302 attendu: utilisateur non identifié")
        self.assertRedirects(response, reverse('login') + "?next=" + url)

    def test_post_import_view_with_superuser(self):
        url = reverse('import1-qcm')
        file_content = "\\documentclass{article}\n\\usepackage{automultiplechoice}\n\\begin{document}\n\\begin{question}{42}question42\\end{question}\n\\end{document}\n"
        simple_file = SimpleUploadedFile("toto.tex", file_content.encode("utf-8"))
        file_data = {"texupload": simple_file}
        data = {"matiere": self.matiere.intitule, "theme": self.theme.sujet, "soustheme": self.soustheme.motcle, "qtype": "S", "texupload": simple_file}
        
        self.client.login(username=self.admin.username, password=self.passwd)
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302, "données valides")
        self.assertRedirects(response, reverse('import2-qcm'))
        self.client.logout()


    def test_get_finalise_import_view_with_no_user(self):
        url = reverse('import2-qcm')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302, "Code HTML 302 attendu: utilisateur non identifié")
        self.assertRedirects(response, reverse('login') + "?next=" + url)

    def test_get_finalise_import_view_with_no_permission_user(self):
        url = reverse('import2-qcm')
        self.client.login(username=self.user.username, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302, "Code HTML 302 attendu: utilisateur identifié sans les droits")
        self.assertRedirects(response, reverse('login') + "?next=" + url)
        self.client.logout()

    def test_get_finalise_import_view_with_permission_user(self):
        url = reverse('import2-qcm')
        self.client.login(username=self.redac.username, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: utilisateur identifié avec les droits")
        self.assertTemplateUsed(response, 'qcmonge/import2_qcm.html', "Le template html n'est pas le bon")
        self.client.logout()

    def test_get_finalise_import_view_with_superuser(self):
        url = reverse('import2-qcm')
        self.client.login(username=self.admin.username, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu: admin")
        self.assertTemplateUsed(response, 'qcmonge/import2_qcm.html', "Le template html n'est pas le bon")
        self.client.logout()

    def test_post_finalise_import_view_with_no_user(self):
        url = reverse('import2-qcm')
        data = {}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302, "Code HTML 302 attendu: utilisateur non identifié")
        self.assertRedirects(response, reverse('login') + "?next=" + url)

    def test_get_finalise_import_view(self):
        url = reverse('import2-qcm')
        data = {}
        self.client.login(username=self.redac, password=self.passwd)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Code HTML 200 attendu")
        self.assertTemplateUsed(response, 'qcmonge/import2_qcm.html', "Le template html n'est pas le bon")
        
        

    @expectedFailure
    def test_post_finalise_import_view(self):
        self.fail("test non écrit")

