from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from qcmonge.models.classement import Matiere

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    matiere_preferee = models.ForeignKey(Matiere, on_delete=models.SET_NULL, null=True, verbose_name='matière préférée')

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
