from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

from qcmonge.models.classement import SousTheme
from texanalizer.texutils import compile_question

import os

from datetime import date

 




class Question(models.Model):
    QUESTION_TYPE = (
        ('S', "question simple"),
        ('M', "question multiple"),
        ('O', "question ouverte"),
    )
    
    qid = models.AutoField(primary_key=True, editable=False)
    qtype = models.CharField(max_length=1, choices=QUESTION_TYPE, verbose_name='type de question')
    enonce = models.TextField(verbose_name='enoncé')
    tex_header = models.TextField(verbose_name='préambule TeX', blank=True)
    fichier_image = models.CharField(max_length=40, editable=False, blank=True, verbose_name="fichier image")
    date_insersion = models.DateField(default=date.today, verbose_name="date d'insersion dans la base")
    auteur = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    thematique = models.ForeignKey(SousTheme, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='thématique')
    valide = models.BooleanField(default=False, verbose_name="question validée par l'utilisateur")
    
    
    def __str__(self):
        return "question " + str(self.qid) + " {" + str(self.thematique) + " ; " + str(self.auteur) + "}"


    def _get_normalised_image_name(self):
        return str(self.qid) + ".png"
    

    def refresh_image(self):
        image = self._get_normalised_image_name() if self.fichier_image == "" else self.fichier_image
        fullname = os.path.join(settings.STATIC_FINAL_IMAGE_DIR, image)
        res = compile_question(self.tex_header, self.enonce, fullname)
        if res:
            if self.fichier_image == "":
                self.fichier_image = image
                self.save()
        else:
            if self.fichier_image != "":
                self.fichier_image = ""
                self.save()
        return res


    def normalise_image_name(self, fullpath_image_name=None):
        normalised_name = self._get_normalised_image_name()
        origin_name = os.path.basename(fullpath_image_name) if fullpath_image_name else self.fichier_image
        if origin_name != "" and origin_name != normalised_name:
            try:
                full_image_path = os.path.join(settings.STATIC_FINAL_IMAGE_DIR, normalised_name)
                os.rename(fullpath_image_name, full_image_path)
            except:
                pass
            self.fichier_image = normalised_name
            self.save()

 
