from django.db import models


class Matiere(models.Model):
    intitule = models.CharField(max_length=30, primary_key=True, verbose_name='intitulé')

    class Meta:
        verbose_name = "matière"

        
    def __str__(self):
        return self.intitule


    
    
    
class Theme(models.Model):
    sujet = models.CharField(max_length=30, primary_key=True)
    domaine = models.ForeignKey(Matiere, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "thème"

        
    def __str__(self):
        return self.sujet + " (" + str(self.domaine) + ")"



    

class SousTheme(models.Model):
    motcle = models.CharField(max_length=30, primary_key=True, verbose_name='mot clé')
    theme = models.ForeignKey(Theme, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "sous-thème"

        
    def __str__(self):
        return self.motcle + " (" + str(self.theme) + ")"

