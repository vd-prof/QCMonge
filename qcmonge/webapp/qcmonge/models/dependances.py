from django.db import models

from qcmonge.models.question import Question


class Fichier(models.Model):
    filename = models.CharField(max_length=100, primary_key=True)
    
    def __str__(self):
        return self.filename




class Filedep(models.Model):
    qid = models.ForeignKey(Question, on_delete=models.CASCADE)
    fid = models.ForeignKey(Fichier, on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.qid) + " utilise le fichier " + str(self.fid)
