from django.http import JsonResponse, Http404
from django.conf import settings
from qcmonge.models.classement import Matiere, Theme, SousTheme
from qcmonge.models.question import Question

import json
import html


"""
Methodes qui retournent des json.
Elles ne sont pas destinées à être appelées par un humain mais par jquery en asynchrone.

Type d'entrée attendu : les données sont encapsulées dans un json qui se trouve
dans le champs 'jsondata' de la requête:
{ 'jsondata': "..." , ... }

Type de sortie attendu : On renvoi un json. Il contient obligatoirement l'attribut 'success'.
Si ce champs est à False, il s'est passé un truc pas normal et le serveur donne la raison dans l'attribut 'reason'.
Si ce champs est à True, les données sont dans l'attribut 'data':
{ 'success': False, 'reason': "..." }
{ 'success': True, 'data': "..." }
"""



def api_structure(request):
    if request.method == "GET":
        raise Http404("GET method not allowed")
    answer = {"success": False}
    data = {}
    nonempty = False
    validated = True
    try:
        postdata = json.loads(request.POST['jsondata'])
        if "nonempty_only" in postdata and postdata["nonempty_only"]:
            nonempty = postdata["nonempty_only"]
        if "even_non_validated" in postdata and postdata["even_non_validated"]:
            validated = False
    except:
        pass
    questions = Question.objects.filter(valide=True) if validated else Question.objects.all()
    matieres = Matiere.objects.all()
    for mat in matieres:
        okmat = (not nonempty) or len(questions.filter(thematique__theme__domaine=mat)) > 0
        if okmat:
            html_mat = html.escape(mat.intitule)
            data[html_mat] = {}
            themes = Theme.objects.filter(domaine=mat).order_by('sujet')
            for thm in themes:
                okthm = (not nonempty) or len(questions.filter(thematique__theme=thm)) > 0
                if okthm:
                    html_thm = html.escape(thm.sujet)
                    sousthemes = SousTheme.objects.filter(theme__domaine=mat, theme=thm).order_by('motcle')
                    data[html_mat][html_thm] = []
                    for sthm in sousthemes:
                        okssthm = (not nonempty) or len(questions.filter(thematique=sthm)) > 0
                        if okssthm:
                            data[html_mat][html_thm].append(html.escape(sthm.motcle))
    answer["data"] = data
    answer["success"] = True
    return JsonResponse(answer)


def api_search_questions(request):
    if request.method == "GET":
        raise Http404("GET method not allowed")
    answer = {"success": False}
    try:
        questions = Question.objects.filter( valide = True)
        postdata = json.loads(request.POST['jsondata'])
        mat = postdata['matiere']
        questions = questions.filter( thematique__theme__domaine = mat )
        thm = postdata['theme']
        questions = questions.filter( thematique__theme = thm )
        sthm = postdata['soustheme']
        questions = questions.filter( thematique = sthm )
        types = postdata['type']
        for t in "SMO":
            if t not in types:
                questions = questions.exclude( qtype = t )
        totquestion = len(questions)
        qlength = max(1, int(postdata['qlength'])) if 'qlength' in postdata else 5
        qoffset = max(0, int(postdata['qoffset'])) if 'qoffset' in postdata else 0
        if totquestion < qlength or qoffset > totquestion:
            qoffset = 0
        questions = questions.order_by('qid')[qoffset:qoffset+qlength]
        data = {}
        data["questions"] = [ { "id": q.qid, "enonce": q.enonce, "image": "/static/" + str(q.fichier_image) } for q in questions]
        data["offset"] = qoffset
        data["total"] = totquestion
        answer["success"] = True
        answer["data"] = data
    except KeyError as e:
        answer["reason"] = "The key " + str(e) + " is missing."
    except IndexError as e:
        answer["reason"] = str(e)
    return JsonResponse(answer)



def api_infos_questions(request):
    if request.method == "GET":
        raise Http404("GET method not allowed")
    answer = {"success": False}
    contexte = {}
    try:
        selected = json.loads(request.POST['jsondata'])
        selected = selected["questions"]
        for q in selected:
            quest = Question.objects.get(qid=q)
            contexte[q] = { "img": "/static/" + quest.fichier_image,
                            "enonce": quest.enonce}
        answer["data"] = contexte
        answer["success"] = True
    except Exception as e:
        answer["reason"] = "Error when finding question on server."
    return JsonResponse(answer)
