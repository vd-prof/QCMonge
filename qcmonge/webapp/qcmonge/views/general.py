from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from qcmonge.models.classement import Matiere, Theme, SousTheme
from qcmonge.models.question import Question
from django.contrib.auth.models import User


"""
    Page par defaut.

    On affiche juste la liste des utilisateurs.

    /!\ A modifier /!\
"""
def index(request):
    return render(request, 'qcmonge/index.html', {'users': User.objects.all()})




"""
    Page de profile d'un utilisateur.

    Vide (ou presque) pour l'instant.
"""
def voir_user(request, user_login):
    user = get_object_or_404(User, username=user_login)
    return HttpResponse("Module de visualisation de l'utilisateur " + user.username)



"""
    Page de visualisation des themes.

    Vide (ou presque) pour l'instant.
"""
def voir_themes(request):
    mtsq = {}
    mats = Matiere.objects.all()
    for m in mats:
        mtsq[m] = {}
        thms = Theme.objects.filter(domaine=m)
        for t in thms:
            mtsq[m][t] = {}
            sthms = SousTheme.objects.filter(theme=t)
            for s in sthms:
                qt = Question.objects.filter(thematique=s)
                qv = qt.filter(valide=True)
                mtsq[m][t][s] = str(len(qv)) + " / " + str(len(qt))
    contexte = {'matieres': mtsq}
    return render(request, 'qcmonge/voir_themes.html', contexte)

