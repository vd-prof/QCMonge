from django.shortcuts import render
from django.http import HttpResponse, Http404

import json
from zipfile import ZipFile, ZIP_DEFLATED
from io import BytesIO

from qcmonge.models.question import Question

from qcmonge.forms.thematique import ThematiqueForm

from texanalizer.texanalyser import SynTexAnal
from texanalizer.texnodes import TexNode, TexTextNode
from texanalizer.textokenizer import TexToken
from texanalizer.texanalyseconstants import TexAnalyseConstants as TAC



"""
    Page de construction de QCM.
    
    On envoie les matières et les thèmes associés à la matière préférée.

    Les sélections et choix sont gérés par ajax (api_*).

"""
def export_qcm(request):
    mat_pref = "---"
    try:
        mat_pref = request.user.profile.matiere_preferee.intitule
    except:
        pass
    form = ThematiqueForm()
    contexte = {'matiere_preferee': mat_pref, 'form': form }
    return render(request, 'qcmonge/export_qcm.html', contexte)



"""
    téléchargement des questions
"""
def get_qcm_content(request):
    if request.method == "GET":
        raise Http404("GET method not allowed")
    try:
        selected = json.loads(request.POST['selected'])
        packages_header = TexNode()
        packages_names = {}
        commands_header = TexNode()
        commands_names = {}
        texcontent = ""
        for grp in selected.keys():
            for k in selected[grp]:
                quest = Question.objects.get(qid=k)
                head = SynTexAnal(quest.tex_header).latex_preambule_part
                cont = SynTexAnal(quest.enonce).latex_document_part
                added_packages = 0
                added_commands = 0
                for line in head:
                    if line.node_type == TAC.COMMAND_NODE and line.name == 'usepackage':
                        pacn = line.params[0].content
                        if pacn not in packages_names:
                            added_packages += 1
                            if added_packages == 1:
                                packages_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN, "% packages for question " + str(k) + "\n")))
                            packages_header.append(line)
                            packages_names[pacn] = line
                        else:
                            my_package = line.rebuild_latex(with_comments=False, after_content=False)
                            their_package = packages_names[pacn].rebuild_latex(with_comments=False, after_content=False)
                            if my_package != their_package:
                                added_packages += 1
                                if added_packages == 1:
                                    packages_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN, "% packages for question " + str(k) + "\n")))
                                packages_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN,"% " + line.rebuild_latex() + "\n")))
                    elif line.node_type & (TAC.NEW_MASK + TAC.RENEW_MASK) > 0:
                        if line.name not in commands_names:
                            added_commands += 1
                            if added_commands == 1:
                                commands_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN, "% commands for question " + str(k) + "\n")))
                            commands_header.append(line)
                        else:
                            my_command = line.rebuild_latex(with_comments=False, after_content=False)
                            their_command = commands_names[line.name]
                            if my_command != their_command:
                                newname = _newname(line.name, commands_names)
                                head.modifyCmdEnvName(line.name, newname, line.node_type)
                                cont.modifyCmdEnvName(line.name, newname, line.node_type)
                                nt = (line.node_type & (TAC.NEW_MASK+TAC.RENEW_MASK)) | TAC.SIMPLE_MASK
                                head.modifyCmdEnvName(line.name, newname, nt)
                                cont.modifyCmdEnvName(line.name, newname, nt)
                                added_commands += 1
                                if added_commands == 1:
                                    commands_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN, "% commands for question " + str(k) + "\n")))
                                commands_header.append(line)
                        commands_names[line.name] = line.rebuild_latex(with_comments=False, after_content=False)
                if added_packages > 0:
                    packages_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN, "\n\n")))
                if added_commands > 0:
                    commands_header.append(TexTextNode(TexToken(TAC.COMMENT_TOKEN, "\n\n")))
                texcontent += _englob_question(grp, k, cont.rebuild_latex(), quest.qtype)
        texheader = packages_header.rebuild_latex() + "\n\n" +  commands_header.rebuild_latex()

        mem_zip = BytesIO()
        with ZipFile(mem_zip, mode="w", compression=ZIP_DEFLATED) as zf:
            zf.writestr("header.tex", texheader)
            zf.writestr("content.tex", texcontent)
        response = HttpResponse(mem_zip.getvalue(), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename="qcm-amc.zip"'
        return response
    except KeyError as e:
        contexte = {
            "errmsg": "Error when generating zip file: The key " + str(e) + " is missing.",
            "matiere_preferee": matiere_preferee(request),
        }
    return render(request, 'qcmonge/export_qcm.html', contexte)



"""
Ceci n'est pas une vue. C'est une fonction annexe.
Elle pernet de trouver un nom d'une commande LaTeX qui n'est 
pas encore pris parmis les commandes déjà disponibles.
"""
def _newname(cmd, cmds):
    n, s = 0, "A"
    while cmd + s in cmds:
        n += 1
        s, p = "", n
        while p > 0:
            s += chr(65+(p%26))
            p = p//26
    return cmd + s

def _englob_question(group_name, qid, content, qtype):
    mult = "mult" if qtype == "M" else ""
    prefix = "" if len(group_name) == 0 else "\\element{" + group_name + "}{\n"
    suffix = "" if len(group_name) == 0 else "\n}"
    return "\n" + prefix + "\\begin{question" + mult + "}{" + str(qid) + "}\n" + content + "\n\\end{question" + mult + "}" + suffix + "\n"
