from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages

from qcmonge.models.question import Question

from qcmonge.forms.importQuestion import QuestionImportForm
from qcmonge.forms.importTex import ImportTexForm
from qcmonge.forms.thematique import NOTHING_SELECTED


"""
    Page d'import de QCM (1/2)

    Cette page nécessite les droits d'écritures de Question.

    On affiche le formulaire d'ajout général (si le questionnaire est homogène)
    et le bouton de téléversement de fichier. Tout ceci se trouve dans le formulaire ImportTexForm.

    On analyse le fichier tex uploadé

    Si tout se passe bien, on avance à la page 2/2
"""
@login_required(login_url=reverse_lazy('login'))
@permission_required('qcmonge.add_question', login_url=reverse_lazy('login'))
def import_qcm(request):
    # Si GET, c'est qu'on vient d'autre part. Si POST, c'est qu'on vient d'ici
    if request.method == "POST":
        form = ImportTexForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            nbnok = form.nb_questions_non_valides
            if nbnok > 0:
                plurs = 's' if nbnok > 1 else ''
                plura = 'ont' if nbnok > 1 else 'a'
                texte = str(nbnok) + " question" + plurs + " n'" + plura + " pas pu être compilée" + plurs + "."
                messages.warning(request, texte)
            return HttpResponseRedirect(reverse_lazy('import2-qcm'))
    else:
        form = ImportTexForm(request.user)

    mat_pref = NOTHING_SELECTED
    try:
        mat_pref = request.user.profile.matiere_preferee.intitule
    except:
        pass
    contexte = {'matiere_preferee': mat_pref, 'form': form }
    return render(request, 'qcmonge/import1_qcm.html', contexte)



"""
    Page d'import de QCM (2/2)

    Affichage des questions trouvées pour validation.
    Ce qui n'a pas été rempli sur la page 1/2 ou dans le tex est à renseigner ici.
"""
@login_required(login_url=reverse_lazy('login'))
@permission_required('qcmonge.add_question', login_url=reverse_lazy('login'))
def finalise_import_qcm(request):
    infos = []
    qids = []
    # Si GET, c'est qu'on vient d'autre part. Si POST, c'est qu'on vient d'ici
    if request.method == "POST":
        qlist = _parse_qids(request.POST["qids"], request.user)
        nb = len(qlist)
        nbgood = 0
        nbsupp = 0
        for pre in qlist:
            prefix = 'q'+str(pre)+'q'
            question = qlist[pre]
            initial = _initial(quest)
            if question:
                formu = QuestionImportForm(question, request.user, request.POST, prefix=prefix, initial=initial)
                if formu.is_valid():
                    if formu.done == formu.DELETED_QUESTION:
                        nbgood += 1
                    if formu.done == formu.UPDATED_QUESTION:
                        nbsupp += 1
        if nbgood > 0:
            plur = "s" if nbgood > 1 else ""
            messages.success(request, str(nbgood) + " question" + plur + " validée" + plur)
        if nbsupp > 0:
            plur = "s" if nbsupp > 1 else ""
            messages.info(request, str(nbsupp) + " question" + plur + " supprimée" + plur)
        if nbgood + nbsupp == nb:
            return HttpResponseRedirect(reverse_lazy('voir-themes'))
    else:
        questions = Question.objects.filter( auteur=request.user ).filter( valide=False ).order_by( 'qid' )
        for (k, quest) in enumerate(questions):
            prefix = 'q'+str(k)+'q'
            data = _initial(quest, prefix+"-")
            initial = _initial(quest)
            qids.append(str(k)+"."+str(quest.qid))
            infos.append( (QuestionImportForm(quest, request.user, data, prefix=prefix, initial=initial), quest.fichier_image, quest.enonce, prefix+'-') )
    mat_pref = NOTHING_SELECTED
    try:
        mat_pref = request.user.profile.matiere_preferee.intitule
    except:
        pass
    contexte = {'matiere_preferee': mat_pref, 'form_img_tex_nom': infos, 'qids': ",".join(qids) }
    return render(request, 'qcmonge/import2_qcm.html', contexte)



def _parse_qids(string, user):
    qids = {}
    qforms = string.split(",")
    for qform in qforms:
        i, qid = qform.split(".")
        try:
            quest = Question.objects.get(qid=qid)
            if quest.auteur == user and not quest.valide:
                qids[i] = quest
        except:
            pass
    return qids
    

def _initial(question, prefix=""):
    initial = {}
    initial[prefix+'qtype'] = question.qtype
    initial[prefix+'keepme'] = True
    initial[prefix+'texheader'] = question.tex_header
    initial[prefix+'texcontent'] = question.enonce
    if question.thematique:
        initial[prefix+'soustheme'] = question.thematique.motcle
        initial[prefix+'theme'] = question.thematique.theme.sujet
        initial[prefix+'matiere'] = question.thematique.theme.domaine.intitule
    return initial

