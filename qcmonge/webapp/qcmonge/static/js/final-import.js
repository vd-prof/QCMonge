
/*
 * Gestion d'une question
 * - gestion de la thématique (QcTheme)
 * - gestion du click pour garder/retirer la question
 */
class QcQuestion {
    constructor(struct, prefix) {
        this.prefix = prefix;
        this.struct = struct;
        this.keepme = null;
        this.qtype = null;
        this.texheader = null;
        this.texcontent = null;
        this.htmlcontent = null;
        this.qctheme = new QcTheme(this.struct, prefix);
        var myself = this;
        $(document).ready(function() { myself.when_ready() });
        this.struct.when_initialized(function() { myself.clickme()} );
    }

    when_ready() {
        this.keepme = document.getElementById("id_"+this.prefix+"keepme");
        this.keepme.checked = true;
        var myself = this;
        this.keepme.addEventListener("change", function(evt) { myself.clickme()} );
        this.qtype = document.getElementById("id_"+this.prefix+"qtype");
        this.texheader = document.getElementById("id_"+this.prefix+"texheader");
        this.texcontent = document.getElementById("id_"+this.prefix+"texcontent");
        this.htmlcontent = document.getElementById("id_"+this.prefix);
        this.qctheme.soustheme_changed = function() { myself.soustheme_changed() };
    }

    clickme() {
        let active = this.keepme.checked;
        this.qctheme.matiere.disabled = !active;
        this.qtype.disabled = !active;
        this.texheader.disabled = !active;
        this.texcontent.disabled = !active;
        if (active) {
            this.soustheme_changed();
        } else {
            this.qctheme.matiere.value = this.qctheme.struct.nil;
            this.qctheme.matiere_changed();
            this.htmlcontent.classList.add("removed_question");
            this.htmlcontent.classList.remove("good_question");
            this.htmlcontent.classList.remove("bad_question");
        }
    }

    soustheme_changed() {
        if (this.qctheme.soustheme.value == this.qctheme.struct.nil) {
            this.htmlcontent.classList.remove("removed_question");
            this.htmlcontent.classList.remove("good_question");
            this.htmlcontent.classList.add("bad_question");
        } else {
            this.htmlcontent.classList.remove("removed_question");
            this.htmlcontent.classList.add("good_question");
            this.htmlcontent.classList.remove("bad_question");
        }
    }
    
}



/*
 * instanciation de tout le bazare.
 * Basée sur le champs caché dont l'id est 'qids'.
 * La valeur est une chaine. Les virgules séparent les questions. Dans une question, un point sépare le prefix de la question et son id.
 */
let qlist = document.getElementById('qids').value.split(",");
let m = qlist.length;
var QCSTRUCT = new QcStruct();
var qcquestions = new Array();
for (let k=0; k<m; k++) {
    let qid = qlist[k].split(".");
    qcquestions.push( new QcQuestion(QCSTRUCT, 'q' + qid[0] + 'q-') );
}

