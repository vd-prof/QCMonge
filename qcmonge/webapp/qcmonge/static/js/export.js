/* id des groupes */
var NB_QGROUPES = -1;


/*
 * Gestion des questions:
 * - forme html dans les questions suggérées
 * - forme html dans les questions sélectionnées
 * - forme html dans les groupes de questions
 */
class Question {
    constructor(item) {
        this.qid = item["id"];
        this.groupe = null;
        this._image_url = item["image"];
        this._enonce = item["enonce"];
        this._proposed = null
        this._selected = null;
        this._grouped = null;
    }

    html_proposed() {
        if (this._proposed == null) {
            let element = document.createElement("DIV");
            element.id = "qid-" + this.qid;
            element.classList.add("question-unique");
            element.innerHTML = '<img src="' + this._image_url + '" alt="' + this._enonce + '">';
            QCMONGE.tire_lache.declare_droppable(element);
            element.addEventListener("dblclick", QCMONGE.double_click_question);
            this._proposed = element;
        }
        return this._proposed;
    }

    html_selected() {
        if (this._selected == null) {
            let element = this.html_proposed().cloneNode(true);
            element.id = "sel-" + this.qid;
            QCMONGE.tire_lache.declare_droppable(element);
            element.addEventListener("dblclick", QCMONGE.double_click_question);
            this._selected = element;
        }
        return this._selected;
    }

    set_group(group) {
        if (this._proposed != null) {
            if (group != null) {
                this._proposed.classList.add("selected");
                this.groupe = group;
            } else {
                this._proposed.classList.remove("selected");
                this.group = null;
            }
        }
    }
    
    html_grouped() {
        if (this._grouped == null) {
            let element = this.html_proposed().cloneNode(true);
            element.id = "grp-" + this.qid;
            QCMONGE.tire_lache.declare_droppable(element);
            this._grouped = element;
        }
        return this._grouped;
    }

    is_selected() {
        return this._proposed.classList.contains("selected");
    }
}



/*
 * Gestion des groupes
 * - forme html d'un groupe
 * - gestions des évènements liées aux groupes
 */
class QGroupe {
    constructor(nom, boutons) {
        this.gid = ++NB_QGROUPES;
        this.name = (nom === undefined ? "groupe-" + this.gid : nom);
        this._controles = (boutons === undefined ? true : boutons);
        this.questions = [];
        this._container = null;
    }

    html_container() {
        if (this._container == null) {
            let element = document.createElement("DIV");
            element.id = "container-" + this.gid;
            element.classList.add("module")
            let content = '<table><caption style="display: flex;"><span style="flex-grow: 1;" id="legend-' + this.gid + '">' + this.name + '</span>';
            if (this._controles) {
                content += '<input type="button" class="capbut" name="showhide-' + this.gid + '" id="showhide-' + this.gid + '" value="Masquer" onclick=\'QCMONGE.showhide_group(' + this.gid + ')\' />';
                content += '<input type="button" class="capbut" name="delete-' + this.gid + '" id="delete-' + this.gid + '" value="Supprimer" onclick=\'QCMONGE.delete_group(' + this.gid + ')\' />';
            }
            content += '</caption><tbody id="content-' + this.gid + '">';
            if (this._controles) {
                content += '<tr><td><label for="name-' + this.gid + '">Nom du groupe :</label>';
                content += '<input type="text" id="name-' + this.gid + '" name="name-' + this.gid + '" value="' + this.name + '" onchange=\'QCMONGE.change_name_group(' + this.gid + ')\' >';
                content += '<span id="err-' + this.gid + '" class="errmsg"></span>';
                content += '</td></tr>';
            }
            content += '<tr><td id="groupe-' + this.gid + '"></td></tr></tbody>';
            element.innerHTML = content;
            QCMONGE.tire_lache.declare_dropzone(element, QCMONGE.drop_in_group);
            this._container = element;
        }
        return this._container;
    }

    change_name() {
        let texte = document.getElementById("name-" + this.gid).value;
        let err = document.getElementById("err-" + this.gid);
        let reg = /^[a-zA-Z][a-zA-Z0-9_-]{0,20}$/;
        if (reg.test(texte)) {
            err.innerHTML = "";
            document.getElementById("legend-" + this.gid).innerHTML = texte;
            this.name = texte;
        } else {
            err.innerHTML = "nom invalide";
        }
    }

    showhide() {
        let lediv = document.getElementById("content-" + this.gid);
        let bouton = document.getElementById("showhide-" + this.gid);
        if (lediv.style.display == "none") {
            bouton.value = "Masquer";
            lediv.style.display = "inline";
        } else {
            bouton.value = "Afficher";
            lediv.style.display = "none";
        }
    }

    add_question(quest) {
        let lediv = document.getElementById("groupe-" + this.gid);
        lediv.appendChild(quest.html_grouped());
        this.questions.push(quest);
        if (quest.groupe != null)
            quest.groupe.questions.splice(quest.groupe.questions.indexOf(quest), 1);
        quest.set_group(this);
    }

    remove_question(quest) {
        let lediv = document.getElementById("groupe-" + this.gid);
        lediv.removeChild(quest.html_grouped());
        this.questions.splice(this.questions.indexOf(quest), 1);
        quest.set_group(null);
    }
    
    delete_me() {
        while (this.questions.length > 0) {
            QCMONGE.groups[0].add_question(this.questions[0]);
        }
        this._container.parentNode.removeChild(this._container);
    }
}







/*
 * Gestion globale de la page
 * - gestion de l'affichage selection/groupes
 * - upload des questions en ajax et affichage
 * - gestion globale des actions et redistribution.
 */
class Qcmonge {
    constructor() {
        this.proposed_questions = {};  // tableau des Questions proposées
        this.groups = [];  // tableau de QGroupe
        this.proposed_container = null;  // html element Id("liste-questions")
        this.selected_container = null;  // html element Id("selected-questions")
        this.groups_container = null;  // html element Id("group-container")
        this.tire_lache = new TireEtLache();
        this.qcstruct = new QcStruct(true);
        this.qctheme = new QcTheme(this.qcstruct);
        var myself = this;
        this.qctheme.soustheme_changed = function() { myself.update_questions(0) };
        $(document).ready(function() { myself.when_ready()} );
    }
    
    when_ready() {
        this.proposed_container = document.getElementById("liste-questions");
        this.selected_container = document.getElementById("selected-questions");
        this.groups.push(new QGroupe("Questions sans groupe", false));
        document.getElementById("groupless").appendChild(this.groups[0].html_container());
        this.groups_container = document.getElementById("group-container");
        this.tire_lache.declare_dropzone(this.proposed_container, this.drop_in_proposition);
        this.tire_lache.declare_dropzone(this.selected_container, this.drop_in_selection);
        this.tire_lache.declare_dropzone(document.getElementById("poubelle"), this.drop_in_trash);
    }

    show_groups() {
        document.getElementById("selection").classList.add("passive_tab");
        document.getElementById("groupement").classList.remove("passive_tab");
    }

    show_selection() {
        document.getElementById("groupement").classList.add("passive_tab");
        document.getElementById("selection").classList.remove("passive_tab");
    }

    
    update_questions(direction) {
        /*
          Si direction == 0, on charge les premières questions
          Si direction == -1, on charge les questions précédentes
          Si direction == +1, on charge les questions suivantes
          Dans tous les cas, on maj les boutons prev/next
        */
        if (document.getElementById("id_soustheme").value == QCMONGE.qcstruct.nil) {
            QCMONGE.async_update_questions()
        } else {
            let nbq = document.getElementById("nbquestion").value;
            let data = {
                matiere: document.getElementById("id_matiere").value,
                theme: document.getElementById("id_theme").value,
                soustheme: document.getElementById("id_soustheme").value,
                type: "",
                qlength: nbq,
                qoffset: Number(document.getElementById("qoffset").value) + direction * nbq
            }
            if (document.getElementById("simpleQ").checked) data["type"] += "S";
            if (document.getElementById("multiQ").checked) data["type"] += "M";
            if (document.getElementById("ouvertQ").checked) data["type"] += "O";
            QCMONGE.qcstruct.postjsondata("search-questions", data, QCMONGE.async_update_questions);
        }
    }

    async_update_questions(data) {
        let qoffset = 0, tot = 0, nbq = 0;
        QCMONGE.proposed_container.innerHTML = "";
        QCMONGE.selected_container.style.minHeight = QCMONGE.proposed_container.offsetHeight + "px";
        if (data != undefined) {
            if (data["success"]) {
                data = data["data"];
                qoffset = data["offset"];
                tot = data["total"];
                nbq = data["questions"].length;
            } else {
                document.getElementById("errmsg").innerHTML = data["reason"];
            }
        }
        for (let k=0; k<nbq; k++) {
            let item = data["questions"][k];
            let qid = item["id"];
            if (!(qid in QCMONGE.proposed_questions))
                QCMONGE.proposed_questions[qid] = new Question(item);
            QCMONGE.proposed_container.appendChild(QCMONGE.proposed_questions[qid].html_proposed());
            QCMONGE.proposed_questions[qid].html_proposed().querySelector("IMG").addEventListener('load', function(e) {
                QCMONGE.selected_container.style.minHeight = QCMONGE.proposed_container.offsetHeight + "px";
            });
        }
        document.getElementById("qoffset").value = qoffset;
        document.getElementById("totquestion").innerHTML = tot;
        document.getElementById("prevbut").disabled = (qoffset == 0);
        document.getElementById("nextbut").disabled = (tot <= qoffset+nbq);
        document.getElementById("allbut").disabled = (tot == 0);
        document.getElementById("razbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
        document.getElementById("viewbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
    }

    select_question(item) {
        let qid = item.id.substring(4);
        let quest = QCMONGE.proposed_questions[qid];
        if (!quest.is_selected()) {
            QCMONGE.selected_container.appendChild(quest.html_selected());
            QCMONGE.groups[0].add_question(quest);
        }
        document.getElementById("razbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
        document.getElementById("viewbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
        document.getElementById("getbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
    }

    unselect_question(item) {
        let qid = item.id.substring(4);
        let quest = QCMONGE.proposed_questions[qid];
        if (quest.is_selected()) {
            quest.groupe.remove_question(quest);
            QCMONGE.selected_container.removeChild(quest.html_selected());
        }
        document.getElementById("razbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
        document.getElementById("viewbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
        document.getElementById("getbut").disabled = (QCMONGE.selected_container.childElementCount == 0);
    }

    select_all_questions() {
        QCMONGE.proposed_container.querySelectorAll(".question-unique").forEach(QCMONGE.select_question);
    }

    unselect_all_questions() {
        QCMONGE.selected_container.querySelectorAll(".question-unique").forEach(QCMONGE.unselect_question);
    }

    drop_in_selection(event, dropped_element, target_dropzone, target_draggable) {
        QCMONGE.select_question(dropped_element);
    }

    drop_in_proposition(event, dropped_element, target_dropzone, target_draggable) {
        QCMONGE.unselect_question(dropped_element);
    }
    
    drop_in_group(event, dropped_element, target_dropzone, target_draggable) {
        let gid = target_dropzone.id.split("-")[1];
        let qid = dropped_element.id.split("-")[1];
        QCMONGE.groups[gid].add_question(QCMONGE.proposed_questions[qid]);
    }

    drop_in_trash(event, dropped_element, target_dropzone, target_draggable) {
        QCMONGE.unselect_question(dropped_element);
    }

    double_click_question(event) {
        event.preventDefault();
        if (QCMONGE.selected_container.contains(this)) {
            QCMONGE.unselect_question(this);
        } else {
            QCMONGE.select_question(this);
        }
    }

    add_group() {
        let newgroup = new QGroupe();
        QCMONGE.groups[newgroup.gid] = newgroup;
        QCMONGE.groups_container.appendChild(newgroup.html_container());
    }

    delete_group(nb) {
        QCMONGE.groups[nb].delete_me();
        delete QCMONGE.groups[nb];
    }

    change_name_group(nb) {
        QCMONGE.groups[nb].change_name();
    }

    showhide_group(nb) {
        QCMONGE.groups[nb].showhide();
    }

    prepare_qcm() {
        let data = {};
        for (let k in QCMONGE.groups) {
            let nom = (k == 0 ? "" : QCMONGE.groups[k].name);
            let tab = [];
            for (let q=0; q<QCMONGE.groups[k].questions.length; q++) {
                tab.push(QCMONGE.groups[k].questions[q].qid);
            }
            if (tab.length > 0) data[nom] = tab;
        }
        console.log(data);
        document.getElementById("selected").value = JSON.stringify(data);
    }

};





/*
 * Gestion du Drag n'drop
 * Deux méthodes utiles: declare_dropzone et declare_droppable
 */
class TireEtLache {
    constructor() {
        this.actions = {};
    }
    
    // les dropzones
    declare_dropzone(element, drop_it_here_function) {
        let me = this;
        element.addEventListener('dragover', function(evt) { me._over_dropzone(evt, element); });
        element.addEventListener('dragleave', function(evt) { me._leave_dropzone(evt, element); });
        element.addEventListener('drop', function(evt) { me._drop_dropzone(evt, element); });
        element.classList.add("droppable-zone");
        this.actions[element.id] = drop_it_here_function;
    }

    _over_dropzone(evt, elmt) {
        elmt.classList.remove("droppable-zone");
        elmt.classList.add("dropped-zone");
        evt.dataTransfer.dropEffect = "move";
        evt.preventDefault();
    }

    _leave_dropzone(evt, elmt) {
        elmt.classList.remove("dropped-zone");
        elmt.classList.add("droppable-zone");
    }

    _drop_dropzone(evt, elmt) {
        evt.preventDefault();
        elmt.classList.remove("dropped-zone");
        elmt.classList.add("droppable-zone");
        let target = document.getElementById(evt.dataTransfer.getData('text/plain'));
        this._drop_it_here(evt, target, elmt, null);
    }
    
    // les droppables
    declare_droppable(element) {
        let me = this;
        element.addEventListener('dragstart', function(evt) { me._start_drag(evt, element); });
        element.addEventListener('dragend', function(evt) { me._end_drag(evt, element); });
        element.addEventListener('drop', function(evt) { me._drop_drag(evt, element); }, true);
        element.classList.add("draggable-obj");
        element.classList.remove("dragged-obj");
        element.draggable = true;
    }

    _start_drag(evt, elmt) {
        evt.dataTransfer.setData('text/plain', elmt.id);
        evt.dataTransfer.effectAllowed = 'move';
        elmt.classList.remove("draggable-obj");
        elmt.classList.add("dragged-obj");
    }

    _end_drag(evt, elmt) {
        elmt.classList.remove("dragged-obj");
        elmt.classList.add("draggable-obj");
    }

    _drop_drag(evt, elmt) {
        evt.preventDefault();
        evt.stopPropagation();
        let dropzone = elmt;
        while (!dropzone.classList.contains("dropped-zone")) {
            dropzone = dropzone.parentElement;
        }
        dropzone.classList.remove("dropped-zone");
        dropzone.classList.add("droppable-zone");
        let target = document.getElementById(evt.dataTransfer.getData('text/plain'));
        this._drop_it_here(evt, target, dropzone, elmt);
    }

    _drop_it_here(event, dropped_element, target_dropzone, target_draggable) {
        if (dropped_element === target_draggable) return;
        if (target_dropzone.contains(dropped_element)) {
            if (target_draggable !== null) {
                if (target_draggable === dropped_element.nextSibling)
                    target_dropzone.insertBefore(target_draggable, dropped_element);
                else
                    target_dropzone.insertBefore(dropped_element, target_draggable);
            } else {
                target_dropzone.appendChild(dropped_element);
            }
        } else {
            for (let cle in this.actions) {
                if (cle === target_dropzone.id) {
                    let fct = this.actions[cle];
                    fct(event, dropped_element, target_dropzone, target_draggable);
                }
            }
        }
    }
}





/*
 * Instanciation de l'objet principale et c'est parti.
 */
var QCMONGE = new Qcmonge();
