/*
 * Gestion de l'ajax en utilisant l'API du serveur.
 *
 * QcStruct stocke aussi la structure de la table du serveur: matière/thème/sous-thème.
 *
 * Il s'occupe de finaliser les initialisations des objets nécessitant que la structure chargée.
 */
class QcStruct {
    constructor(nonempty_only) {
        this.nonempty_only = (nonempty_only == undefined) ? false : nonempty_only;
        this._initialized = false;
        this.structure = {};
        this.nil = "---";
        this.csrf = "";
        this._jobs = new Array();
        var myself = this;
        $(document).ready(function() { myself.when_ready() });
    }

    postjsondata(endurl, data, callback) {
        let sentdata = {
            csrfmiddlewaretoken: this.csrf,
            jsondata: JSON.stringify(data)
        };
        $.post("/qcmonge/api/"+endurl+"/", sentdata, callback);
    }

    when_ready() {
        var myself = this;
        this.csrf = document.getElementsByName("csrfmiddlewaretoken")[0].value;
        this.postjsondata("structure", {"nonempty_only": this.nonempty_only}, function(data){
            if (data["success"]) {
                myself.structure  = data["data"];
                myself._initialized = true;
                while (myself._jobs.length > 0) {
                    myself._jobs.shift()();
                }
            } else {
                document.getElementById("errmsg").innerHTML = data["reason"];
            }
        });
    }

    when_initialized(fct) {
        if (this._initialized) {
            fct();
        } else {
            this._jobs.push(fct);
        }
    }
    
}






/*
 * Gestion des listes déroulantes matière/thème/sous-thème.
 */
class QcTheme {
    constructor(struct, prefix) {
        this.prefix = (prefix == undefined) ? "" : prefix;
        this.struct = struct;
        var myself = this;
        struct.when_initialized(function() { myself.when_ready()} );
    }

    when_ready() {
        this.matiere = document.getElementById("id_" + this.prefix + "matiere");
        this.theme = document.getElementById("id_" + this.prefix + "theme");
        this.soustheme = document.getElementById("id_" + this.prefix + "soustheme");
        let select_content = "<option value=\"" + this.struct.nil + "\">" + this.struct.nil + "</option>\n";
        for (let k in this.struct.structure) {
            select_content += "<option value=\"" + k + "\">" + k + "</option>\n";
        }
        this.matiere.innerHTML = select_content;
        if (document.getElementById("id_matiere_preferee")) {
            this.matiere.value = document.getElementById("id_matiere_preferee").value;
        }
        this.matiere_changed();
        this.theme_changed();
        var myself = this;
        this.matiere.addEventListener('change', function(evt) { myself.matiere_changed()} );
        this.theme.addEventListener('change', function(evt) { myself.theme_changed()} );
        this.soustheme.addEventListener('change', function(evt) { myself.soustheme_changed()} );
    }

    matiere_changed() {
        let nullval = (this.matiere.value == this.struct.nil);
        this.theme.disabled = nullval;
        if (nullval) {
            this.theme.value = this.struct.nil;
        } else {
            let select_content = "<option value=\"" + this.struct.nil + "\">" + this.struct.nil + "</option>\n";
            let mat = this.struct.structure[this.matiere.value];
            for (var k in mat) {
                select_content += "<option value=\"" + k + "\">" + k + "</option>\n";
            }
            let tval = this.theme.value;
            if (!(tval in mat)) {
                tval = this.struct.nil;
            }
            this.theme.innerHTML = select_content;
            this.theme.value = tval;
        }
        this.theme_changed();
    }

    theme_changed() {
        let nullvar = (this.theme.value == this.struct.nil);
        this.soustheme.disabled = nullvar;
        if (nullvar) {
            this.soustheme.value = this.struct.nil;
        } else {
            let select_content = "<option value=\"" + this.struct.nil + "\">" + this.struct.nil + "</option>\n";
            let thm = this.struct.structure[this.matiere.value][this.theme.value];
            for (let k=0; k<thm.length; k++) {
                select_content += "<option value=\"" + thm[k] + "\">" + thm[k] + "</option>\n";
            }
            let sstval = this.soustheme.value;
            if (!(sstval in thm)) {
                sstval = this.struct.nil;
            }
            this.soustheme.innerHTML = select_content;
            this.soustheme.value = sstval;
        }
        this.soustheme_changed();
    }

    soustheme_changed() {
    }
};
