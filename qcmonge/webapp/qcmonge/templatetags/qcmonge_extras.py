from django import template

from qcmonge.models.question import Question

register = template.Library()

def need_valide(value):
    qlist = Question.objects.filter(valide=False).filter(auteur__username=value)
    return len(qlist)

register.filter('need_valide', need_valide)
