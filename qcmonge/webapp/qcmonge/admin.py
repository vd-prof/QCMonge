from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

from qcmonge.models.classement import Matiere, Theme, SousTheme
from qcmonge.models.question import Question
from qcmonge.models.dependances import Fichier
from qcmonge.models.general import Profile



class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Généralités", {'fields': ['thematique', 'auteur', 'date_insersion', 'valide']}),
        ("Question", {'fields': ['qtype', 'tex_header', 'enonce', 'fichier_image']}),
        ]
    readonly_fields = ('date_insersion', 'fichier_image',)

    def get_form(self, request, obj=None, **kwargs):
        form = super(QuestionAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['auteur'].initial = request.user
        return form

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        save_me = False
        if form.cleaned_data['thematique'] == None:
            obj.valide = False
            save_me = True
        if obj.auteur == None:
            obj.auteur = request.user
            save_me = True
        if save_me:
            obj.save()
        if not obj.valide:
            self.message_user(request, "question non validée: aucune thématique", level=messages.WARNING)
    
    
    # options d'affichage de la liste des questions
    list_display = ('qid', 'qtype', 'thematique', 'auteur', 'date_insersion', 'valide', 'fichier_image', 'enonce')
    list_filter = ['qtype', 'thematique', 'auteur', 'date_insersion']
    search_fields = ['enonce', 'tex_header']

    # ajout d'une action de refresh image par lot
    actions = ['refresh_image_group']
    def refresh_image_group(self, request, queryset):
        nb, ntot = 0, len(queryset)
        for obj in queryset:
            nb += 1 if obj.refresh_image() else 0
        level = messages.SUCCESS if ntot == nb else messages.WARNING
        plur = 's' if nb > 1 else ''
        self.message_user(request, str(nb) + '/' + str(ntot) + ' image' + plur + ' de question actualisée' + plur, level=level)
    refresh_image_group.short_description = "regénérer LaTeX"

    # ajout d'un bouton pour single refresh image
    change_form_template = "qcmonge/change_question.html"
    def response_change(self, request, obj):
        if "_refresh-latex-image" in request.POST:
            if obj.refresh_image():
                self.message_user(request, 'Image actualisée', level=messages.SUCCESS)
            else:
                self.message_user(request, "Erreur d'actualisation de l'image", level=messages.ERROR)
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)


class ThemeAdmin(admin.ModelAdmin):
    # options d'affichage
    list_display = ('sujet', 'domaine')
    list_filter = ['domaine']


class SousThemeAdmin(admin.ModelAdmin):
    # options d'affichage
    list_display = ('motcle', 'theme', 'get_domaine')
    list_filter = ['theme', 'theme__domaine']
    def get_domaine(self, obj):
        return obj.theme.domaine
    get_domaine.short_description = "Matière"



class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline,)

    def __init__(self, *args, **kwargs):
        super(UserAdmin, self).__init__(*args, **kwargs)
        CustomUserAdmin.list_display = ['username', 'email', 'first_name', 'last_name', 'is_staff', 'redacteur', 'last_login', 'questions_valides', 'questions_en_attente']

    def redacteur(self, obj):
        return obj.has_perm('qcmonge.add_question')
    redacteur.boolean = True

    def questions_valides(self, obj):
        return len(Question.objects.filter(auteur=obj, valide=True))

    def questions_en_attente(self, obj):
        return len(Question.objects.filter(auteur=obj, valide=False))

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request,obj)


admin.site.site_url = "/qcmonge"
admin.site.site_header = "Administration de QCMonge"
admin.site.site_title = "Administration de QCMonge"
admin.site.index_title = "Accueil"
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Matiere)
admin.site.register(Theme, ThemeAdmin)
admin.site.register(SousTheme, SousThemeAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Fichier)
