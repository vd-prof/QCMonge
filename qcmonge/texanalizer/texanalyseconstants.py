"""
Constantes utilisées pour l'analyse.

Fichier devenu nécessaire pour éviter les imports cycliques.
C'est moche, je sais, mais je n'avais pas envie de tout coller dans un seul fichier
comme un gros cochon...
"""

class TexAnalyseConstants(object):

    #
    #
    # Constantes des noeuds de l'arbre abstrait d'un document LaTeX
    #
    #
    
    # types
    COMMAND_NODE = 12 # 12 =  8 +  4
    ENVIRONMENT_NODE = 20 # 20 = 16 +  4
    NEWCOMMAND_NODE = 10 # 10 =  8 +  2
    NEWENVIRONMENT_NODE = 18 # 18 = 16 +  2
    RENEWCOMMAND_NODE = 9  #  9 =  8 +  1
    RENEWENVIRONMENT_NODE = 17 # 17 = 16 +  1
    GENERAL_NODE = 32
    PARAMETER_NODE = 64
    OPTION_NODE = 128

    
    # masks
    ENV_MASK = 16
    CMD_MASK = 8
    SIMPLE_MASK = 4
    NEW_MASK = 2
    RENEW_MASK = 1


    STRING_NODE_TYPES = {
        COMMAND_NODE : "Command",
        ENVIRONMENT_NODE : "Environment",
        NEWCOMMAND_NODE : "New Command",
        NEWENVIRONMENT_NODE : "New Environment",
        RENEWCOMMAND_NODE : "Re Ne Command",
        RENEWENVIRONMENT_NODE : "Re New Environment",
        GENERAL_NODE : "General",
        PARAMETER_NODE : "Parameter",
        OPTION_NODE : "Option",
    }

    


    #
    #
    # Constantes des tokens d'un document LaTeX
    #
    #
        
    COMMAND_TOKEN = 256 # 2^8
    NEWSOMETHING_TOKEN = 768 # 2^8 + 2^9
    COMMENT_TOKEN = 1 # 2^0
    OPEN_ENVIRONMENT_TOKEN = 384 # 2^8 + 2^7
    CLOSE_ENVIRONMENT_TOKEN = 320 # 2^8 + 2^6
    OPEN_PARAM_TOKEN = 168 # 2^7 + 2^5 + 2^3
    CLOSE_PARAM_TOKEN = 104 # 2^6 + 2^5 + 2^3
    OPEN_OPT_TOKEN = 152 # 2^7 + 2^4 + 2^3
    CLOSE_OPT_TOKEN = 88 # 2^6 + 2^4 + 2^3
    QCMONGE_TOKEN = 3 # 2^1 + 2^0
    TEXT_TOKEN = 4 # 2^2
    END_FILE_TOKEN = 0

    """
    Ces valeurs ont été choisies pour une utilisation de masques.
    bit | correspondance
    --------------------
     0  | commentaire
     1  | balise QCMONGE
     2  | texte
     3  | séparateur
     4  | option
     5  | paramètre
     6  | fin de quelque chose
     7  | début de quelque chose
     8  | commande
     9  | (re)new quelquechose

    Ainsi le token { est un séparateur de début de paramètre donc son type est 2^3 + 2^5 + 2^7 = 168
    """

    # prefix de toutes les balises destinées à QCMonge.
    # Elles font partie des commentaires LaTeX.
    # Elles ont toutes la même forme:
    # %%-QCMONGE-matiere: blabla
    # %%-QCMONGE-theme: blabla
    # %%-QCMONGE-sous-theme: blabla    
    QCMONGE_COMMENT_PREFIX = "%%-QCMONGE-"
        
    
    # Certaines commandes sont "fausses": Elles ne sont que des racourcis et n'ont pas besoin d'être analysées.
    # elles ne sont formées que d'un seul caractère, parmi la liste suivante.
    FAKE_COMMANDS = "{}[]% ,&;#$_\\"


    # pour affichage de debug
    STRING_TOKEN_TYPES = {
        COMMAND_TOKEN: "CMD",
        NEWSOMETHING_TOKEN: "NEW",
        COMMENT_TOKEN: "COM",
        OPEN_ENVIRONMENT_TOKEN: "OEV",
        CLOSE_ENVIRONMENT_TOKEN: "CEV",
        OPEN_PARAM_TOKEN: "OPA",
        CLOSE_PARAM_TOKEN: "CPA",
        OPEN_OPT_TOKEN: "OOP",
        CLOSE_OPT_TOKEN: "COP",
        QCMONGE_TOKEN: "QCM",
        TEXT_TOKEN: "TXT",
        END_FILE_TOKEN: "EOF"
    }

    SEPARATORS = {
        "{": OPEN_PARAM_TOKEN,
        "}": CLOSE_PARAM_TOKEN,
        "[": OPEN_OPT_TOKEN,
        "]": CLOSE_OPT_TOKEN
    }

    BLANKS = "\n \t\r"
    
    NEWSOMETHING = {
        "newcommand": NEWCOMMAND_NODE,
        "renewcommand": RENEWCOMMAND_NODE,
        "newenvironment": NEWENVIRONMENT_NODE,
        "renewenvironment": RENEWENVIRONMENT_NODE
    }



    COMMANDS_WITHOUT_PARAMETER = ["infty",]


    EXCLUDED_PACKAGES = ['inputenc', 'fontenc', 'automultiplechoice']


    #
    #
    # Constantes pour les erreurs d'analyse
    #
    #
    
    UNDEFINED_ERROR = 0
    NO_INITIAL_COMMAND_ERROR = 1
    NO_DOCUMENTCLASS_ERROR = 2
    WRONG_NUMBER_OF_PARAMETERS_ERROR = 3
    WRONG_TYPE_OF_PARAMETER_ERROR = 4
    NO_DOCUMENT_FOUND_ERROR = 5
    NO_ENVIRONMENT_NAME_ERROR = 6
    WRONG_TYPE_OF_ENVIRONMENT_NAME_ERROR = 7
    NO_CLOSING_PARAMETER_ERROR = 8
    ENVIRONMENT_END_NOT_FOUND_ERROR = 9
    NO_CLOSING_OPTION_ERROR = 10
    WRONG_ENVIRONMENT_NAME_ERROR = 11
    NO_PARAMETER_FOUND_ERROR = 12


    ERROR_MESSAGES = {
        UNDEFINED_ERROR : "Undefined LaTeX analysis error",
        NO_INITIAL_COMMAND_ERROR : "No initial command",
        NO_DOCUMENTCLASS_ERROR : "First command is not '\\documentclass'",
        WRONG_NUMBER_OF_PARAMETERS_ERROR : "Wrong number of parameters",
        WRONG_TYPE_OF_PARAMETER_ERROR : "Wrong type of parameter",
        NO_DOCUMENT_FOUND_ERROR : "No 'document' environment found",
        NO_ENVIRONMENT_NAME_ERROR : "No environment name",
        WRONG_TYPE_OF_ENVIRONMENT_NAME_ERROR : "Wrong type of environment",
        NO_CLOSING_PARAMETER_ERROR : "char '}' not found",
        ENVIRONMENT_END_NOT_FOUND_ERROR : "End environment not found",
        NO_CLOSING_OPTION_ERROR : "char ']' not found",
        WRONG_ENVIRONMENT_NAME_ERROR : "Wrong environment name",
        NO_PARAMETER_FOUND_ERROR : "No parameter found",
    }
    
