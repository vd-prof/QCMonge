from texanalizer.texanalyseconstants import TexAnalyseConstants as TAC
from texanalizer.texanalyser import SynTexAnal as TexAnal

from subprocess import check_call, CalledProcessError, DEVNULL
import os


HERE = os.path.dirname(os.path.abspath(__file__))
TEMPLATE = os.path.join(HERE, "minimal.tex")


"""
data: le document latex sous forme de chaine.

sortie: liste de questions.
Chaque question est un dictionnaire:
{ 'type':S/M/O, 'enonce':latex, 'tex_header':latex, 'soustheme':texte, 'theme':texte, 'matiere':texte}
Le texte est None si l'info n'est pas trouvée
"""
def extract_questions(data, exclude_packages=None):
    question_list = []
    if exclude_packages == None:
        exclude_packages = []
    latexdoc = TexAnal(data).full_latex_document
    for node in latexdoc.get_node_list("question"):
        ouverte = node.get_texCommandNode_list("AMCOpen", TAC.COMMAND_NODE)
        qtype = 'O' if len(ouverte) > 0 else 'S'
        infos = _extract_infos(node, qtype, latexdoc, exclude_packages)
        question_list.append(infos)
    for node in latexdoc.get_node_list("questionmult"):
        infos = _extract_infos(node, 'M', latexdoc, exclude_packages)
        question_list.append(infos)
    return question_list
        

def _extract_infos(node, qtype, latexdoc, excludes):
    theenonce = node.content.rebuild_latex(False)
    theheader = latexdoc.get_header_from_node(node.content, excludes)
    qcm = node.qcmonge_comments
    themat = qcm['matiere'] if 'matiere' in qcm else None
    thetheme = qcm['theme'] if 'theme' in qcm else None
    thesoustheme = qcm['sous-theme'] if 'sous-theme' in qcm else None
    qdic = {'type': qtype,
            'enonce': theenonce,
            'tex_header': theheader,
            'matiere': themat,
            'theme': thetheme,
            'soustheme': thesoustheme}
    return qdic



def compile_question(header, content, destfic):
    texfile = destfic + ".tex"
    destdir = os.path.dirname(destfic)
    
    with open(TEMPLATE, "r") as template:
        texte = template.read()

    texte = texte.replace("<<<header>>>", header)
    texte = texte.replace("<<<content>>>", content)
    
    with open(texfile, "w") as output:
        output.write(texte)
    
    compilok = True
    cmd_tex_array = [ "pdflatex", "-interaction=nonstopmode", "-output-directory=" + destdir, texfile ]
    cmd_png_array = [ "convert", "-density", "300", destfic+".pdf", "-quality", "90", destfic ]
    try:
        # check_call(cmd_tex_array) # pour afficher la sortie de compilation
        check_call(cmd_tex_array, stdout=DEVNULL) # sans affichage
        check_call(cmd_png_array)
    except CalledProcessError:
        compilok = False

    basefile = os.path.basename(destfic) + "."
    tempfiles = [f for f in os.listdir(destdir) if os.path.basename(f)[:-3] == basefile]
    for fic in tempfiles:
        lefic = os.path.join(destdir, fic)
        try:
            os.remove(lefic)
        except:
            pass

    return compilok
