from texanalizer.texanalyseconstants import TexAnalyseConstants as TAC

class TexToken(object):
    """
    Classe des tokens lexicaux pour l'analyse de fichiers LaTeX.

    C'est une classe purement data, pas de logique dedans.
    """



    def __init__(self, token_type, content="", index=-1, data=""):
        self.token_type = token_type
        self.content = content
        self.index = index
        self.data = data
        self.after_content = ""












class TexTokenizer(object):
    """
    Séparation en lexèmes lexicaux
    """

    def __init__(self, latex_content):
        self.__index = 0
        self.__latex_data = latex_content
        self.token = TexToken(token_type=TAC.END_FILE_TOKEN, index=self.__index, data=self.__latex_data)
        self.token = self.step_to_next_token()

    @property
    def __current_char(self):
        return self.__latex_data[self.__index]

    def step_to_next_token(self):
        comments = True
        while comments:
            if self.__index >= len(self.__latex_data):
                if self.token.token_type != TAC.END_FILE_TOKEN:
                    self.token = TexToken(token_type=TAC.END_FILE_TOKEN,
                                          index=self.__index,
                                          data=self.__latex_data)
                return self.token
            car = self.__current_char
            if car == "\\":
                new_token = self.__getCommande()
            elif car == "%":
                new_token = self.__getCommentaire()
            elif car in TAC.SEPARATORS:
                new_token = TexToken(token_type=TAC.SEPARATORS[car],
                                     content=car,
                                     index=self.__index,
                                     data=self.__latex_data)
                self.__index += 1
            else:
                new_token = self.__getTexte()
            # prise en compte des blancs
            after = ""
            while self.__index < len(self.__latex_data) and \
                  self.__latex_data[self.__index] in TAC.BLANKS:
                after += self.__latex_data[self.__index]
                self.__index += 1
            new_token.after_content = after
            comments = new_token.token_type == TAC.COMMENT_TOKEN
            if comments:
                self.token.after_content += "\n"
            self.token = new_token
        return self.token



    def __getCommande(self):
        init_index = self.__index
        if self.__latex_data[self.__index+1] in TAC.FAKE_COMMANDS:
            return self.__getTexte()
        content = ""
        self.__index += 1
        try:
            car = self.__current_char
            while ord('a') <= ord(car) <= ord('z') or ord('A') <= ord(car) <= ord('Z'):
                content += car
                self.__index += 1
                car = self.__current_char
            if car == '*': # on accepte les commandes étoilées
                content += car
                self.__index += 1
        except IndexError:
            pass
        ttype = TAC.COMMAND_TOKEN
        if content == "begin":
            ttype = TAC.OPEN_ENVIRONMENT_TOKEN
        elif content == "end":
            ttype = TAC.CLOSE_ENVIRONMENT_TOKEN
        elif content in TAC.NEWSOMETHING:
            ttype = TAC.NEWSOMETHING_TOKEN
        return TexToken(token_type=ttype,
                        content=content,
                        index=init_index,
                        data=self.__latex_data)


    def __getCommentaire(self):
        init_index = self.__index
        content = ""
        try:
            while self.__current_char != "\n":
                content += self.__current_char
                self.__index += 1
            self.__index += 1
        except IndexError:
            pass
        ttype = TAC.COMMENT_TOKEN
        if content[:len(TAC.QCMONGE_COMMENT_PREFIX)] == TAC.QCMONGE_COMMENT_PREFIX:
            ttype = TAC.QCMONGE_TOKEN
            content = content[len(TAC.QCMONGE_COMMENT_PREFIX):]
        return TexToken(token_type=ttype,
                        content=content,
                        index=init_index,
                        data=self.__latex_data)


    def __getTexte(self):
        init_index = self.__index
        content = ""
        try:
            separators = "\\%" + "".join(TAC.SEPARATORS)
            while True:
                if self.__current_char not in separators:
                    content += self.__current_char
                    self.__index += 1
                elif self.__current_char == "\\" and self.__latex_data[self.__index+1] in TAC.FAKE_COMMANDS:
                    content += self.__latex_data[self.__index] + self.__latex_data[self.__index+1]
                    self.__index += 2
                else:
                    break
        except IndexError:
            pass
        return TexToken(token_type=TAC.TEXT_TOKEN,
                        content=content,
                        index=init_index,
                        data=self.__latex_data)




if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        fic = open(sys.argv[1], 'r', encoding="utf_8")
        data = fic.read()
        fic.close()
        tt = TexTokenizer(data)
        while tt.token.token_type != TAC.END_FILE_TOKEN:
            print("####\t"+str(tt._TexTokenizer__index)+"\ttoken: " + \
                  TAC.STRING_TOKEN_TYPES[tt.token.token_type] + \
                  "\t" + tt.token.content)
            tt.step_to_next_token()
    else:
        print("il manque le nom du fichier LaTeX à analyser...")
        
