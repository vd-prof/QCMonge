from texanalizer.texanalyseconstants import TexAnalyseConstants as TAC

"""
Voici les différents noeuds de l'arbre abstrait d'un fichier LaTeX.
Les noms sont normalement explicites...

Il y a 4 classes :
  * TexNode représente un noeud de l'arbre abstrait, avec ses fils.
  * TexTextNode représente une feuille contenant du texte
        ou des trucs non signifiants pour nous
  * TexCommandeNode représente une commande ou un environment LaTeX
        avec ses éventuels options, paramètres et contenus.

On a enfin TexDocRoot qui représente la racine d'un arbre abstrait.
Il possède une entete (le documentclass), un préambule ainsi que 
le contenu proprement dit du fichier (les données).
"""

    
class TexNode(object):
    """
    Un noeud de l'arbre semantique d'un fichier LaTeX.
    On précise s'il est un noeud général, un noeud d'une option ou d'un paramètre.

    Cette classe implemente quelques fonctionnalites des listes:
    noeud = TexNode()
    len(noeud) # nombre de fils de noeud
    noeud[1] # donne le 2eme fils de noeud
    noeud.append(fils) # ajout un nouveau fils
    """
    
    def __init__(self, childs=None, node_type=None):
        self.__childs = childs if childs != None else []
        self.node_type = node_type if node_type != None else TAC.GENERAL_NODE
        self.delimiters = None


    def __getitem__(self, index):
        return self.__childs[index]

    def __len__(self):
        return len(self.__childs)

    def __str__(self):
        return self.arbre()
    
    def append(self, value):
        self.__childs.append(value)


    def arbre(self, prefix="", issubtree=False):
        node = "General"
        if self.node_type == TAC.PARAMETER_NODE:
            node = "Parameter"
        if self.node_type == TAC.OPTION_NODE:
            node = "Option"
        new_prefix = "│  " if issubtree else "   "
        msg = prefix + ("├" if issubtree else "└") + "──┬▷ [" + node + " Node]\n"
        for k, child in enumerate(self.__childs):
            issubsubtree = k < len(self.__childs) - 1
            msg += child.arbre(prefix+new_prefix, issubsubtree)
        return msg
    
    def rebuild_latex(self, with_comments=True, after_content=True):
        latex_prefix, latex, latex_suffix = "", "", ""
        if self.delimiters:
            if after_content:
                deplie = lambda x:x.content + x.after_content
            else:
                deplie = lambda x:x.content
            latex_prefix, latex_suffix = map(deplie, self.delimiters)
        for c in self.__childs:
            latex += c.rebuild_latex(with_comments, after_content)
        return latex_prefix + latex + latex_suffix

    def get_definition(self, text):
        for c in self.__childs:
            r = c.get_definition(text)
            if r:
                return r
        return None

    @property
    def qcmonge_comments(self):
        nodes = {}
        for c in self.__childs:
            nodes.update(c.qcmonge_comments)
        return nodes

    def get_texCommandNode_list(self, name, node_type):
        nodes = []
        for c in self.__childs:
            nodes += c.get_texCommandNode_list(name, node_type)
        return nodes

    def get_used_cmd_env_set(self, node_type):
        ret = set()
        for c in self.__childs:
            ret = ret.union(c.get_used_cmd_env_set(node_type))
        return ret

    def modifyCmdEnvName(self, oldname, newname, node_type):
        for c in self.__childs:
            c.modifyCmdEnvName(oldname, newname, node_type)

            
    """
    def getEnvCmdNameSet(self, mask):
        cmds = set()
        for c in self.childs:
            cmds |= c.getEnvCmdNameSet(mask)
        return cmds
    """







class TexTextNode(object):
    """
    Du texte dans l'arbre semantique d'un fichier LaTeX.
    C'est une feuille de l'arbre.
    Pour nous, c'est quelque chose qui n'a pas de signification
    mais il peut y en avoir pour LaTeX. Par exemple, ce texte peut
    contenir des formules mathématiques (pourvu que cette formule
    n'utilise pas de commande ni d'environment).

    Ce texte peut aussi être un commentaire LaTeX destiné a QCMonge.
    """

    def __init__(self, token):
        self.token = token

    def __str__(self):
        return self.arbre()

    @property
    def content(self):
        return self.token.content

    @property
    def qcmonge(self):
        return self.token.token_type == TAC.QCMONGE_TOKEN
    
    def arbre(self, prefix="", issubtree=False):
        text = "QCMonge" if self.qcmonge else "Text"
        return prefix + ("├" if issubtree else "└") + "─── [" + text + "] " + self.content + "\n"

    def rebuild_latex(self, with_comments=True, after_content=True):
        text = ""
        if not self.qcmonge or with_comments:
            text = self.content
            if self.qcmonge:
                text = TAC.QCMONGE_COMMENT_PREFIX + text + "\n"
            if after_content:
                text += self.token.after_content
        return text
    
    def get_definition(self, text):
        return None

    @property
    def qcmonge_comments(self):
        if self.qcmonge:
            splitit = self.token.content.split(':', 1)
            decomp = map(lambda x:x.strip(), splitit)
            return {decomp[0].lower(): decomp[1]}
        return {}

    def get_texCommandNode_list(self, name, node_type):
        return []

    def get_used_cmd_env_set(self, node_type):
        return set()

    def modifyCmdEnvName(self, oldname, newname, node_type):
        pass

    """
    def getEnvCmdNodeList(self, text, node_type):
        return []
    """









class TexCommandNode(object):
    """
    Une commande ou un environment dans un fichier LaTeX.
    C'est un noeud particulier dans l'arbre semantique du fichier.
    """

    def __init__(self, token, node_type):
        self.token = token
        self.node_type = node_type
        self._args = []
        self.content = None
        self.delimiters = []

    def __str__(self):
        return self.arbre()

    @property
    def name(self):
        return self.token.content
    
    @property
    def param_count(self):
        return sum(1 if c.node_type == TAC.PARAMETER_NODE else 0 for c in self._args)

    @property
    def opt_count(self):
        return sum(1 if c.node_type == TAC.OPTION_NODE else 0 for c in self._args)

    @property
    def params(self):
        return tuple(arg if len(arg) > 1 else arg[0] for arg in self._args if arg.node_type == TAC.PARAMETER_NODE)

    @property
    def opts(self):
        return tuple(arg if len(arg) > 1 else arg[0] for arg in self._args if arg.node_type == TAC.OPTION_NODE)

    def set_arguments(self, arguments):
        self._args = arguments

    
    def arbre(self, prefix="", issubtree=False):
        msg = "Command" if self.node_type & TAC.CMD_MASK else "Environment"
        if self.node_type & TAC.NEW_MASK:
            msg = "New" + msg
        if self.node_type & TAC.RENEW_MASK:
            msg = "Re" + msg
        msg = prefix + ("├" if issubtree else "└") + "──" + ("┬" if self._args or self.content else "─") + " [" + msg + "] " + self.name + "\n"
        new_prefix = "│  " if issubtree else "   "
        if self._args:
            for k, arg in enumerate(self._args):
                issubsubtree = (k < len(self._args)-1) or self.content
                msg += arg.arbre(prefix + new_prefix, issubsubtree)
        if self.content:
            msg += prefix + new_prefix + "├▷ [content]\n"
            msg += self.content.arbre(prefix + new_prefix)
        return msg

    def rebuild_latex(self, with_comments=True, after_content=True):
        prefix, suffix = "", ""
        if after_content:
            deplie = lambda x:x.content + x.after_content
        else:
            deplie = lambda x:x.content
        str_delim = list(map(deplie, self.delimiters))
        if self.node_type == TAC.COMMAND_NODE:
            prefix += "\\" + self.name
        elif self.node_type == TAC.ENVIRONMENT_NODE:
            prefix += "\\begin" + str_delim[0] + self.name + str_delim[1]
            suffix = "\\end" + str_delim[2]+ self.name + str_delim[3] + suffix
            if self.content:
                suffix = self.content.rebuild_latex(with_comments, after_content) + suffix
        else:
            prefix += "\\"
            prefix += "re" if self.node_type & TAC.RENEW_MASK > 0 else ""
            prefix += "new"
            prefix += "command" if self.node_type & TAC.CMD_MASK > 0 else "environment"
            prefix += str_delim[0]
            prefix += "\\" if self.node_type & TAC.CMD_MASK > 0 else ""
            prefix += self.name + str_delim[1]
        latex = ""
        if self._args:
            for arg in self._args:
                latex += arg.rebuild_latex(with_comments, after_content)
        reconstruc = prefix + latex + suffix
        if after_content:
            reconstruc += self.token.after_content
        return reconstruc

    def get_definition(self, text):
        if self.node_type & (TAC.NEW_MASK + TAC.RENEW_MASK) > 0 and self.name == text:
            return self
        return None

    @property
    def qcmonge_comments(self):
        nodes = {}
        if self._args:
            for c in self._args:
                nodes.update(c.qcmonge_comments)
        if self.content:
            nodes.update(self.content.qcmonge_comments)
        return nodes

    def modifyCmdEnvName(self, oldname, newname, node_type):
        if self.node_type == node_type and self.name == oldname:
            self.token.content = newname
        if self._args:
            for c in self._args:
                c.modifyCmdEnvName(oldname, newname, node_type)
        if self.content:
            self.content.modifyCmdEnvName(oldname, newname, node_type)

    def get_texCommandNode_list(self, name, node_type):
        ret = []
        if self.node_type == node_type and self.name == name:
            return [self]
        elif self.node_type & TAC.SIMPLE_MASK > 0:
            for arg in self._args:
                ret += arg.get_texCommandNode_list(name, node_type)
            if self.content:
                ret += self.content.get_texCommandNode_list(name, node_type)
        return ret

    def get_used_cmd_env_set(self, node_type):
        ret = set()
        if self.node_type == node_type:
            ret = ret.union([self.name])
        if self.node_type & TAC.SIMPLE_MASK > 0:
            for arg in self._args:
                ret = ret.union(arg.get_used_cmd_env_set(node_type))
            if self.content:
                ret = ret.union(self.content.get_used_cmd_env_set(node_type))
        return ret
        






class TexDocRoot(object):
    """
    La racine de l'arbre semantique d'un fichier LaTeX.
    """
    
    def __init__(self):
        self.documentclass = None
        self.preambule = None
        self.content = None

    def __str__(self):
        return self.arbre()

    
    def arbre(self, prefix="", issubtree=False):
        msg = ""
        if self.documentclass:
            msg += prefix + ("├" if self.preambule or self.content else "└") + "──┬▷ [classdoc]\n"
            new_prefix = "│  " if self.preambule or self.content else "   "
            msg += self.documentclass.arbre(prefix + new_prefix)
        if self.preambule:
            msg += prefix + ("├" if self.content else "└") + "──┬▷ [preambule]\n"
            new_prefix = "│  " if self.content else "   "
            msg += self.preambule.arbre(prefix + new_prefix)
        if self.content:
            msg += prefix + "└──┬▷ [document]\n"
            msg += self.content.arbre(prefix + "   ")
        return msg
    
    def rebuild_latex(self, with_comments=True, after_content=True):
        latex = ""
        if self.documentclass:
            latex += self.documentclass.rebuild_latex(with_comments, after_content)
        if self.preambule:
            latex += self.preambule.rebuild_latex(with_comments, after_content)
        if self.content:
            latex += self.content.rebuild_latex(with_comments, after_content)
        return latex

    def get_definition_node(self, text):
        defini = None
        if self.preambule:
            defini = self.preambule.get_definition(text)
        if not defini and self.content:
            defini = self.content.get_definition(text)
        return defini

    @property
    def package_list(self):
        if not self.preambule:
            return []
        nodes = []
        for c in self.preambule:
            if c.node_type == TAC.COMMAND_NODE and c.name == "usepackage":
                nodes.append({"name":c.params[0].content, "opt":c.opts[0].content if c.opts else ""})
        return nodes

    def get_header_from_node(self, node, excluded_packages):
        text = ""
        if self.preambule:
            for pac in self.package_list:
                if pac['name'] not in excluded_packages:
                    text += "\\usepackage"
                    if len(pac['opt']) > 0:
                        text += "[" + pac['opt'] + "]"
                    text += "{" + pac['name'] + "}\n"
            for cmd in node.get_used_cmd_env_set(TAC.COMMAND_NODE):
                definition = self.get_definition_node(cmd)
                if definition:
                    text += definition.rebuild_latex(False, False) + "\n"
            for env in node.get_used_cmd_env_set(TAC.ENVIRONMENT_NODE):
                definition = self.get_definition_node(env)
                if definition:
                    text += definition.rebuild_latex(False, False) + "\n"
        return text

    def modifyCmdEnvName(self, oldname, newname, node_type):
        if self.documentclass:
            self.documentclass.modifySmdEnvName(oldname, newname, node_type)
        if self.preambule:
            self.preambule.modifySmdEnvName(oldname, newname, node_type)
        if self.content:
            self.content.modifySmdEnvName(oldname, newname, node_type)

    """
    def getEnvCmdNameSet(self, mask):
        cmds = set()
        if self.preambule:
            cmds |= self.preambule.getEnvCmdNameSet(mask)
        if self.document:
            cmds |= self.document.getEnvCmdNameSet(mask)
        return cmds
    """    

    def get_node_list(self, environment_name):
        if self.content:
            return self.content.get_texCommandNode_list(environment_name, TAC.ENVIRONMENT_NODE)
        return []
    

