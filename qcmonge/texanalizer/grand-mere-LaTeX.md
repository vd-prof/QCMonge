Grammaire d'annalyse de TeX
===========================

Voici la grammaire de fichier Tex que je propose.

En majuscule, le vocabulaire non-terminal, en minuscule le vocabulaire terminal.

 * Fichier -> Entete Preambule Document StarBlabla oef

 * Entete -> Commande(documentclass)
 * Preambule -> StarCommande
 * Document -> Environment(document)

 * StarCommande -> eps | Commande StarCommande
 * StarOption -> eps | Option StarOption
 * StarParametre -> eps | Parametre StarParametre
 * StarBlabla -> texte StarBlabla | Commande StarBlabla | Environment StarBlabla

 * Environment -> Commande(begin) { texte } StarParametre Option StarBlabla Commande(end) { texte }
 * Commande -> commande Option StarParametre | commande(newcommand|newenvironment) Parametre StarOption StarParametre 
 * Option -> eps | [ StarBlabla ]
 * Parametre -> { StarBlabla }

 * texte
 * commande
 * oef


Bien sûr, puisque c'est pour la vraie vie, il y a des exceptions.

D'abord, il y a de fausses commandes. Par exemple, `\{` ou encore `\_`. On les considère donc comme du texte.

Ensuite, la phrase `l'intervalle ]2;+\infty[ est ouvert` va planter car le `[` après `\infty` sera considéré comme le début d'une option
et l'analyseur n'en trouvera pas la fin... La grammaire étant non contextuelle, on a opté pour une liste (nécessairement non exhaustive)
qui répertorie les commandes qui n'ont pas d'option. Le crochet fermant sera donc considéré comme du texte.

Enfin, la commande `\newenvironment` peut poser problème. En effet, elle est définie par `\newcommand{nom}{prefixe}{suffixe}`.
Or le préfixe peut contenir un `\begin{machin}` et le suffixe `\end{machin}`.
Dans ce cas, la grammaire voit le début d'un environment et ne trouve pas la fin car elle tombe sur la fin du prefixe avant...
Il a donc fallu faire une exception et pouvoir considérer `\begin{machin}` comme une commande et non un début d'environment.

Il y a enocre un soucis non résolu: la grammaire suppose que le préambule n'est constitué que de commandes, avec des options et des paramètres.
Or la commande `\def` ne suit pas vraiement ce modèle...


Arbre abstrait
==============

Voici différentes parties de l'arbre abstrait.

D'abord, une commande
```
\nomCommande[une option]{un parametre}{un deuxième parametre}


└──┬ [Command] nomCommande
   ├──┬▷ [Option Node]
   │  └─── [Text] une option
   ├──┬▷ [Parameter Node]
   │  └─── [Text] un parametre
   └──┬▷ [Parameter Node]
      └─── [Text] un deuxième parametre
```

Puis un environnement
```
\begin{nomEnvironment}[une option]{un parametre}
Du texte, \CommandeSimple et encore du texte.
\end{nomEnvironment}


└──┬ [Environment] nomEnvironment
   ├──┬▷ [Option Node]
   │  └─── [Text] une option
   ├──┬▷ [Parameter Node]
   │  └─── [Text] un parametre
   ├▷ [content]
   └──┬▷ [General Node]
      ├─── [Text] Du texte,
      ├─── [Command] CommandeSimple
      └─── [Text] et encore du texte.
```

Et un document entier:
```
\documentclass[10pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\begin{document}
Un document \LaTeX presque vide.
\begin{center}
VIDE !
\end{center}
\end{document}


├──┬▷ [classdoc]
│  └──┬ [Command] documentclass
│     ├──┬▷ [Option Node]
│     │  └─── [Text] 10pt,a4paper
│     └──┬▷ [Parameter Node]
│        └─── [Text] article
├──┬▷ [preambule]
│  └──┬▷ [General Node]
│     ├──┬ [Command] usepackage
│     │  ├──┬▷ [Option Node]
│     │  │  └─── [Text] utf8x
│     │  └──┬▷ [Parameter Node]
│     │     └─── [Text] inputenc
│     └──┬ [Command] usepackage
│        ├──┬▷ [Option Node]
│        │  └─── [Text] T1
│        └──┬▷ [Parameter Node]
│           └─── [Text] fontenc
└──┬▷ [document]
   └──┬ [Environment] document
      ├▷ [content]
      └──┬▷ [General Node]
         ├─── [Text] Un document
         ├─── [Command] LaTeX
         ├─── [Text] presque vide.
         └──┬ [Environment] center
            ├▷ [content]
            └──┬▷ [General Node]
               └─── [Text] VIDE !
```


Schémas fonctionnel
===================

Shémas de dépendance des fonctions utilisées concrètement pour la grammaire.
Une flèche `A ───> B` signifie que la fonction `A` utilise la fonction `B`.

```
fichier
  │││
  ││╰───> entete         ╭───────────────────────────────────────╮
  ││         │           ⴸ                                       │
  ││         ╰─────> commande ───────────────> parametre <────╮  │
  ││                    ⴷ │                         ⴷ │       │  │
  │╰─> préambule ───────╯ ╰────> starParamOpt ──────╯ ╰───> starBlabla ────> starTexte
  │                                  ⴷ │                      ⴷ ⴷ │
  │          ╭─────> environment ────╯ ╰─────> option ────────╯ │ │
  │          │           ⴷ  │                                   │ │
  │          │           │  ╰───────────────────────────────────╯ │
  ╰───> document         ╰────────────────────────────────────────╯
```
