from texanalizer.texanalyseconstants import TexAnalyseConstants as TAC
from texanalizer.texnodes import *
from texanalizer.textokenizer import *



class SynTexAnalError(SyntaxError):
    
    def __init__(self, errno, token):
        self.errno = errno
        self.token = token

    def error_message(self, nb_ln_before=3, nb_ln_after=2):
        idx = self.token.index
        msg = TAC.ERROR_MESSAGES[self.errno] + "\n"
        nbln = 1 + self.token.data[:idx].count('\n')
        minchar, k = idx, 0
        while minchar >= 0 and k < nb_ln_before:
            k += 1 if self.token.data[minchar] == '\n' else 0
            minchar -= 1
        maxchar, k = idx, 0
        while minchar < len(self.token.data) and k < nb_ln_after:
            k += 1 if self.token.data[maxchar] == '\n' else 0
            maxchar += 1
        ln = 1 + self.token.data[:minchar].count('\n')
        smaxln = len(str(1 + self.token.data[:maxchar].count('\n')))
        msg += ("▶" if ln == nbln else str(ln)).rjust(smaxln) + "."
        for c in self.token.data[minchar:maxchar]:
            msg += c
            if c == '\n':
                ln += 1
                msg += ("▶" if ln == nbln else str(ln)).rjust(smaxln) + "."
        return msg


    def __str__(self):
        return self.error_message()









"""
décorateur pour debug de SynTexAnal
"""
PRINT_DEBUG = False
DEBUG_STACK_LEVEL = 0
from functools import wraps

def deco_print_debug(funct):
    @wraps(funct)
    def wrapper(*args, **kwargs):
        global PRINT_DEBUG, DEBUG_STACK_LEVEL
        if PRINT_DEBUG:
            print(" "*(3*DEBUG_STACK_LEVEL) + ">>> " + funct.__name__ + " ###" + "\ttoken: (pos: " +
                  str(args[0].textoken._TexTokenizer__index) + ", type: " +
                  TAC.STRING_TOKEN_TYPES[args[0].textoken.token.token_type] + ") " + args[0].textoken.token.content)
        DEBUG_STACK_LEVEL += 1
        output = funct(*args, **kwargs)
        DEBUG_STACK_LEVEL -= 1
        if PRINT_DEBUG:
            print(" "*(3*DEBUG_STACK_LEVEL) + "--- " + funct.__name__ + "\n" + str(output))
        return output
    return wrapper












"""
Fabrication de l'arbre abstrait.

Pour la grammaire, voir la doc.
"""
class SynTexAnal(object):        

    def __init__(self, latex_data):
        self.textoken = TexTokenizer(latex_data)
        self.__texDoc = None
        self.analysis_error = None
        

    @property
    def full_latex_document(self):
        return self.__generate_analysis(self.__fichier)

    @property
    def latex_document_part(self):
        return self.__generate_analysis(self.__starBlabla)

    @property
    def latex_preambule_part(self):
        return self.__generate_analysis(self.__preambule)

    def __generate_analysis(self, entry_funct):
        if self.__texDoc == None and self.analysis_error == None:
            # Si document pas généré et pas d'erreur, on lance l'analyse
            try:
                self.__texDoc = entry_funct()
            except SynTexAnalError as stae:
                self.analysis_error = stae
        if self.analysis_error != None:
            raise self.analysis_error
        return self.__texDoc

    
    

    @property
    def __current_token_type(self):
        return self.textoken.token.token_type

    def __step_to_next_token(self):
        return self.textoken.step_to_next_token()

    

    #
    # Fichier -> Entete Preambule Document *Blabla EOF
    #
    # return TexDocRoot
    @deco_print_debug
    def __fichier(self):
        thedoc = TexDocRoot()
        thedoc.documentclass = self.__entete()
        thedoc.preambule = self.__preambule()
        thedoc.content = self.__document()
        # ici il y a peut-être encore des trucs à parser au niveau syntaxe... mais on s'en fout pour l'instant.
        return thedoc


    #
    # Entete -> Commande(documentclass)
    #
    # return TexCommandNode
    @deco_print_debug
    def __entete(self):
        if self.__current_token_type != TAC.COMMAND_TOKEN:
            raise SynTexAnalError(TAC.NO_INITIAL_COMMAND_ERROR, self.textoken.token)
        docl = self.__commande()
        if docl.name != "documentclass":
            raise SynTexAnalError(TAC.NO_DOCUMENTCLASS_ERROR, self.textoken.token)
        if docl.param_count != 1:
            raise SynTexAnalError(TAC.WRONG_NUMBER_OF_PARAMETER_ERROR, self.textoken.token)
        if not isinstance(docl.params[0], TexTextNode):
            raise SynTexAnalError(TAC.WRONG_TYPE_OF_PARAMETER_ERROR, self.textoken.token)
        return docl



    #
    # Preambule -> *Commande
    #
    # return TexNode
    @deco_print_debug
    def __preambule(self):
        nodes = []
        while self.__current_token_type in [TAC.COMMAND_TOKEN, TAC.NEWSOMETHING_TOKEN]:
            nodes.append(self.__commande())
        return TexNode(childs=nodes)



    #
    # Document -> Environment(document)
    #
    # return TexCommandNode
    @deco_print_debug
    def __document(self):
        if self.__current_token_type != TAC.OPEN_ENVIRONMENT_TOKEN:
            raise SynTexAnalError(TAC.NO_DOCUMENT_FOUND_ERROR, self.textoken.token)
        envnode = self.__environment()
        if envnode.name != "document":
            raise SynTexAnalError(TAC.WRONG_ENVIROMENT_NAME_ERROR, self.textoken.token)
        return envnode



    #
    # *Blabla -> *Qcmonge *Texte *Commande *Environment *Blabla
    #
    # return TexNode
    @deco_print_debug
    def __starBlabla(self, end_separator=None, treat_environment_as_command=False):
        end_separator = end_separator if end_separator != None else TAC.END_FILE_TOKEN
        nodes = []
        while self.__current_token_type != TAC.END_FILE_TOKEN:
            if self.__current_token_type == TAC.QCMONGE_TOKEN:
                nodes.append( TexTextNode(self.textoken.token) )
                self.__step_to_next_token()
            elif self.__current_token_type == TAC.TEXT_TOKEN:
                nodes.append( self.__starTexte(end_separator) )
            elif self.__current_token_type in [TAC.COMMAND_TOKEN, TAC.NEWSOMETHING_TOKEN] or \
                 (treat_environment_as_command and self.__current_token_type in [TAC.OPEN_ENVIRONMENT_TOKEN, TAC.CLOSE_ENVIRONMENT_TOKEN]):
                nodes.append( self.__commande() )
            elif self.__current_token_type == TAC.OPEN_ENVIRONMENT_TOKEN:
                nodes.append( self.__environment() )
            elif self.__current_token_type == TAC.OPEN_PARAM_TOKEN:
                # Ce n'est pas un vrai parametre, mais pour nous, c'est pareil.
                # C'est par exemple {\textbf toto}
                nodes.append( self.__parametre() )
            elif self.__current_token_type == TAC.OPEN_OPT_TOKEN:
                # ça ressemble à un début d'option mais ce n'est pas le cas.
                # Par exemple ]1;\infty[
                nodes.append( self.__starTexte(end_separator) )
            else:
                break
        return TexNode(childs=nodes)



    #
    # *Texte -> Texte
    #
    # return TexTextNode
    @deco_print_debug
    def __starTexte(self, end_separator=None):
        """
        ici tous les délimiteurs non-précédés d'une commande ou
        d'un environment sont du texte. On peut donc append tout ça.
        """
        end_separator = end_separator if end_separator != None else TAC.END_FILE_TOKEN
        tok = self.textoken.token
        toktype = self.__step_to_next_token().token_type
        while toktype != TAC.END_FILE_TOKEN and \
              toktype & TAC.COMMAND_TOKEN == 0 and \
              toktype != TAC.QCMONGE_TOKEN and \
              toktype != TAC.OPEN_PARAM_TOKEN and \
              toktype != end_separator:
            tok.content += tok.after_content + self.textoken.token.content
            tok.after_content = self.textoken.token.after_content
            toktype = self.__step_to_next_token().token_type
        tok.token_type = TAC.TEXT_TOKEN
        return TexTextNode(tok)



    #
    # *ParamOpt -> eps | Parametre *ParamOpt | Option *ParamOpt
    #
    # return TexNode
    @deco_print_debug
    def __starParamOpt(self, treat_environment_as_command=False):
        nodes = []
        added = True
        while added:
            added = False
            while self.__current_token_type == TAC.OPEN_OPT_TOKEN:
                nodes.append( self.__option() )
                added = True
            while self.__current_token_type == TAC.OPEN_PARAM_TOKEN:
                nodes.append( self.__parametre(treat_environment_as_command) )
                added = True
        return nodes



    #
    # Environment -> Commande(begin) { Texte } *ParamOpt *Blabla Commande(end) { Texte }
    #
    # return TexCommandNode
    @deco_print_debug
    def __environment(self):
        delimiters = []
        if self.__step_to_next_token().token_type != TAC.OPEN_PARAM_TOKEN:
            raise SynTexAnalError(TAC.NO_ENVIRONMENT_NAME_ERROR, self.textoken.token)
        delimiters.append(self.textoken.token)
        if self.__step_to_next_token().token_type != TAC.TEXT_TOKEN:
            raise SynTexAnalError(TAC.WRONG_TYPE_OF_ENVIRONMENT_NAME_ERROR, self.textoken.token)
        te = TexCommandNode(self.textoken.token, TAC.ENVIRONMENT_NODE)
        if self.__step_to_next_token().token_type != TAC.CLOSE_PARAM_TOKEN:
            raise SynTexAnalError(TAC.NO_CLOSING_PARAMETER_ERROR, self.textoken.token)
        delimiters.append(self.textoken.token)
        self.__step_to_next_token()
        te.set_arguments( self.__starParamOpt() )
        te.content = self.__starBlabla()
        if self.__current_token_type != TAC.CLOSE_ENVIRONMENT_TOKEN:
            raise SynTexAnalError(TAC.ENVIRONMENT_END_NOT_FOUND_ERROR, self.textoken.token)
        if self.__step_to_next_token().token_type != TAC.OPEN_PARAM_TOKEN:
            raise SynTexAnalError(TAC.WRONG_NUMBER_OF_PARAMETERS_ERROR, self.textoken.token)
        delimiters.append(self.textoken.token)
        if self.__step_to_next_token().token_type != TAC.TEXT_TOKEN or self.textoken.token.content != te.name:
            raise SynTexAnalError(TAC.ENVIRONMENT_END_NOT_FOUND_ERROR, self.textoken.token)
        if self.__step_to_next_token().token_type != TAC.CLOSE_PARAM_TOKEN:
            raise SynTexAnalError(TAC.NO_CLOSING_PARAMETER_ERROR, self.textoken.token)
        delimiters.append(self.textoken.token)
        self.__step_to_next_token()
        te.delimiters = delimiters
        return te



    #
    # Commande -> commande Options *Parametre
    #
    # return TexCommandNode
    @deco_print_debug
    def __commande(self, treat_environment_as_command=False):
        if self.__current_token_type == TAC.NEWSOMETHING_TOKEN:
            cmdtype = TAC.NEWSOMETHING[self.textoken.token.content]
            self.__step_to_next_token()
            namenode = self.__parametre()
            if len(namenode) != 1:
                raise SynTexAnalError(TAC.WRONG_NUMBER_OF_PARAMETERS_ERROR, self.textoken.token)
            if cmdtype & TAC.CMD_MASK > 0:
                # c'est un {re,}newcommand
                if not isinstance(namenode[0], TexCommandNode):
                    raise SynTexAnalError(TAC.WRONG_TYPE_OF_PARAMETER_ERROR, self.textoken.token)
                tcn = TexCommandNode(namenode[0].token, cmdtype)
                tcn.delimiters = namenode.delimiters
            else:
                # c'est un {re,}newenvironment
                if not isinstance(namenode[0], TexTextNode):
                    raise SynTexAnalError(TAC.WRONG_TYPE_OF_PARAMETER_ERROR, self.textoken.token)
                tcn = TexCommandNode(namenode[0].token, cmdtype)
                tcn.delimiters = namenode.delimiters
                treat_environment_as_command=True
        else:
            tcn = TexCommandNode(self.textoken.token, TAC.COMMAND_NODE)
            self.__step_to_next_token()
        if tcn.name not in TAC.COMMANDS_WITHOUT_PARAMETER:
            tcn.set_arguments( self.__starParamOpt(treat_environment_as_command) )
        return tcn



    #
    # Options -> eps | [ *Blabla ]
    #
    # return TexNode
    @deco_print_debug
    def __option(self):
        if self.__current_token_type != TAC.OPEN_OPT_TOKEN:
            return None
        delimiters = [self.textoken.token]
        self.__step_to_next_token()
        opt = self.__starBlabla( TAC.CLOSE_OPT_TOKEN )
        if self.__current_token_type != TAC.CLOSE_OPT_TOKEN:
            raise SynTexAnalError(TAC.NO_CLOSING_OPTION_ERROR, self.textoken.token)
        delimiters.append(self.textoken.token)
        self.__step_to_next_token()
        opt.node_type = TAC.OPTION_NODE
        opt.delimiters = delimiters
        return opt



    #
    # Parametre -> { *Blalba }
    #
    # return TexNode
    @deco_print_debug
    def __parametre(self, treat_environment_as_command=False):
        if self.__current_token_type != TAC.OPEN_PARAM_TOKEN:
            raise SynTexAnalError(TAC.NO_PARAMETER_FOUND_ERROR, self.textoken.token)
        delimiters = [self.textoken.token]
        self.__step_to_next_token()
        param = self.__starBlabla( end_separator=TAC.CLOSE_PARAM_TOKEN, treat_environment_as_command=treat_environment_as_command )
        if self.__current_token_type != TAC.CLOSE_PARAM_TOKEN:
            raise SynTexAnalError(TAC.NO_CLOSING_PARAMETER_ERROR, self.textoken.token)
        delimiters.append(self.textoken.token)
        self.__step_to_next_token()
        param.node_type = TAC.PARAMETER_NODE
        param.delimiters = delimiters
        return param




    

    

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        fic = open(sys.argv[1], 'r', encoding="utf_8")
        data = fic.read()
        fic.close()
        sta = SynTexAnal(data)
        doc = sta.full_latex_document
        if doc:
            #print("\nTout est ok\n")
            #print("######################################################")
            #print(doc)
            #print("######################################################")
            #print(doc.rebuild_latex())
            #print("######################################################")
            print(doc.packages)
        else:
            print("\nça a planté quelque part.\n")
            print(sta.analysis_error.error_message());
    else:
        print("il manque le nom du fichier LaTeX à analyser...")




