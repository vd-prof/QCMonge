
Les javascript
==============

On synthétise ici le rôle de chaque fichier js: ce qu'il fait ou permet de faire ainsi que les fonctions principales.






jquery-3.3.1.min.js
-------------------

LE jquerry. Je l'ai mis là pour ne pas avoir à le recharger tout le temps et pour pouvoir travailler sans réseau.
J'avoue que j'aimerai m'en passer: je n'aime pas utiliser une bibliothèque sans vraiment la connaître.
Dans mon souvenir, elle n'est là que pour le côté simple des requètes ajax.






thematique.js
-------------


### classe QcTheme ###

En charge de gérer les matières / thèmes / sous-thèmes.

Attributs:
 * `structure`: la structure de la base en terme de matière / thème / sous-thème
 * `nil`: symbolise un non-choix
 * `csrf`: cookie de django.
 * `prefix`: le préfixe des id des balises matière / thème / sous-thème
 * `matiere`, `theme`, `soustheme`: les objets html en rapport avec leur id.

Méthodes:
 * `postjsondata(endurl, data, callback)`: pour effectuer les requètes api.
 * `when_ready()`: finalisation de l'initialisation
 * `matiere_changed()`, `theme_changed()`, `soustheme_changed()`: gestion des listes.
 
 
 
 



export.js
---------

### classe Question ###

Attributs:
 * `qid`: integer
 * `groupe`: QGroupe
 
 Méthodes:
 * `constructor(item)`: item est un dico avec les clés id, image et enonce.
 * `html_proposed()`: configure les events drop et dblclick
 * `html_selected()`: configure les events drop et dblclick
 * `html_grouped()` : configure les events drop
 * `set_group(group)`: màj le groupe et le proposed
 * `is_seleted()`


### classe QGroupe ###

Attributs:
 * `gid`: integer
 * `name`: string
 * `questions`: Array(Question)
 
Méthodes:
 * `constructor(nom, boutons)`: nom:string et boutons:boolean sont optionels
 * `html_container()`: configure les events drop
 * `change_name()`
 * `showhide()`
 * `add_question(quest)`: màj le groupe, la page et la question
 * `remove_question(quest)`: màj le groupe, lapage et la question
 * `delete_me()`: màj les questions et la page


### classe Qcmonge ###

Attributs:
 * `qctheme`: l'objet `QcTheme` en charge des thèmes
 * `proposed_questions`: dico{id:Question}
 * `groups`: Array(QGroupe)
 * `proposed_container`: noeud
 * `selected_container`: noeud
 * `groups_container`: noeud
 * `tire_lache`: TireEtLache
 
Méthodes:
 * `when_ready()`: finalisation l'init de QCMONGE, de la page et configuration des drop
 * `show_groups()`
 * `show_selection()`
 * `update_questions(direction)`: lance la màj des questions
 * `async_update_questions(data)`: màj la page
 * `select_question(item)`
 * `unselect_question(item)` : deselect_question(item)
 * `select_all_questions()` : all_questions()
 * `unselect_all_questions()` : raz_questions()
 * `drop_in_selection(event, dropped_element, target_dropzone, target_draggable)`
 * `drop_in_proposition(event, dropped_element, target_dropzone, target_draggable)`
 * `drop_in_group(event, dropped_element, target_dropzone, target_draggable)`
 * `drop_in_trash(event, dropped_element, target_dropzone, target_draggable)`
 * `double_click_question(event)`
 * `add_group()`
 * `delete_group(gid)`
 * `change_name_group(gid)`
 * `showhide_group(gid)`
 * `prepare_qcm()`: rassemble les id des questions sélectionnées dans un champs caché `selected`.


### TireEtLache ###

Attributs:
 * `actions`
 
Méthodes:
 * `declare_dropzone(element, drop_it_here_function)`
 * `declare_droppable(element)`





importation.js
--------------

Ne fait qu'instancier un objet QcTheme pour la gestion des thèmes.




final-import.js
---------------

### classe QcQuestion ###

gestion d'une question.

Attributs:
 * `prefix` : en cas de multiples questions
 * `keepme`, `qtype`, `texheader`, `texcontent` : les objets html en rappot avec leur id.
 * `htmlcontent`: le conteneur de toute la question
 * `qctheme` : l'objet QcTheme en charge de la gestion des thèmes.
 
Méthodes:
 * `when_ready()` : initialisation de toute la partie html. Elle s'execute quand tout est prêt.
 * `clickme()` : gestion de la case à coher `keepme`.
 * `soustheme_changed()`: gestion de la liste de sous-thème (css).


Le js récupère l'information sur le nombre de question et les préfixes de chacune et initialise autant de QcQuestion que nécessaire.
