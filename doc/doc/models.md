Structure de la base de données
===============================

Voici la liste des tables ainsi que leurs champs.

Conventions:
 * Les champs avec une étoile sont des clés pimaires.
 * Chaine(n) signifie «_chaine de caractères de taille n_».
 * int signifie «_nombre entier_».
 * etrange(blabla) signifie «_clé etrangère vers la table blabla_».

Quand il n'y a pas de clé primaire, django en ajoute une automatiquement. Il s'agit alors d'un entier nommé `id`.




Classement
----------


### Matiere #
| nom du champ  | `intitule`(*) |
|-|:-:|
| type du champ | chaine(30)  |

L'`intitule` est le nom de la matière.



### Theme #
| nom du champ  | `sujet`(*)   | `domaine`          |
|-|:-:|:-:|
| type du champ | chaine(30) | etrange(Matiere) |

Le nom du thème est dans le `sujet`. Chaque thème est rattaché à une matière.


### SousTheme #
| nom du champ  | `motcle`(*)  | `theme`          |
|-|:-:|:-:|
| type du champ | chaine(30) | etrange(Theme) |

Le nom du sous-thème est dans `motcle`. Chaque sous-thème est rattaché à un thème.





question
--------


### Question #
| nom du champ  | `qid`(*)  | `qtype`      | `enonce` | `tex_header` | `fichier_image` | `date_insersion` | `auteur` | `thematique` | `valide` |
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| type du champ | int     | 'S', 'M' ou 'O' | chaine | chaine   | chaine(40) | date           | etrange(User) | etrange(SousTheme) | booléen |

Une question est identifiée par son `qid`. Elle a un `enonce` et un `tex_header` qui permet de la compiler.
Elle est simple (une seule bonne réponse), multiple (plusieurs bonnes réponses possibles) ou ouverte.
Cette information est dans `qtype`.
Le chemin de son `image` est stocké en base. C'est un chemin relatif par rapport au dossier général des données.
On stocke aussi son `auteur` et sa `date_insersion` pour un éventuel historique.
Pour la recherche, on enregistre son sous-thème dans `thematique`.
Le sous-thème étant relié à un thème et une matière, on peut retrouver l'ensemble des informations de la question.

Lors de l'import, les questions trouvées dans le fichier LaTeX sont stockées en base mais pas validées.
Elles sont `valide`s quand l'utilisateur a confirmé la `thematique` de la question, lors de la phase de vérification de l'utilisateur.

La classe python qui représente une question possède une méthode statique `getConcatTexHeader` qui permet
de concatener les préambules de plusieurs questions sans importer plusieurs fois le même package et sans que
les newcommand des différentes questions se télescopent.




dependances
-----------


### Fichier #
| nom du champ  | `filename`(*) |
|-|:-:|
| type du champ | chaine(100) |

Chaque ligne représente un fichier annexe comme une image ou un pdf nécessaire à une question.


### Filedep #
| nom du champ  | `qid`   | `fid` |
|-|:-:|:-:|
| type du champ | etrange(Question) | etrange(Fichier) |

Crée un lien entre un fichier et une question LaTeX.




general
-------


### Profile #
| nom du champ  | `user`          | `matiere_preferee`  |
|-|:-:|:-:|
| type du champ | etrange(User) | etrange(Matiere)  |

Ajoute une matière préférée à un utilisateur.
Ceci permet d'afficher directement la bonne matière lors de la recherche ou l'insersion de questions.

La table `User` est créée par Django.

