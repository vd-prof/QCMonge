# fonctionnement de django


Voici un rapide survol du fonctionnement de django.
Plus d'info dans la doc (très bien faite) de django lui-même : <https://docs.djangoproject.com/fr/2.0>
section "Premier pas".


J'ai planqué des trucs sous le tapis pour essayer d'être le plus simple possible. Que les puristes me pardonnent.


Sommairement, django est basé sur un modèle MVT (modèle, vue, template).
Ceci signifie que les données (modèle), leurs traitements (vue) et l'affichage (template) sont dissociés dans
des endroits distincts du code.


Dans toutes les explications qui vont suivre, nous allons nous référer à différents fichiers.
Le répertoire de base que nous utiliserons est `QCMonge/qcmonge`.
Nous l'appellerons BASEDIR pour faire plus court.


Nous allons suivre le trajet d'une requête web.
L'utilisateur souhaite accéder à la page _qcmonge/themes_ pour avoir des informations sur les thèmes saisis
sur le serveur. Avec le serveur de test, il aurait tapé l'adresse <http://localhost:8000/qcmonge/themes>


1. Django commence par charger le ficher contenant les déclarations des urls : `BASEDIR/webapp/qcmonge/urls.py`.
   Quand il accède à ce fichier, django a supprimé la première partie de l'url. Il ne reste plus que la fin,
   c'est-à-dire `themes`. Il voit alors que cette url est branchée à la fonction `views.voir_themes` et qu'elle
   s'appelle `themes`.

2. Django charge donc le fichier des vues qui correspond, ici `BASEDIR/webapp/qcmonge/views.py`.
   Dans ce fichier se trouvent tous les traitements et manipulations de la base de données.
   On voit que la fonction `voir_themes` ne fait pas grand chose.
   D'ailleurs, si on regarde les autres fonctions de vues, elles ne font pas grand chose non plus.
   Elles s'occupent de récupérer ou de sauvegarder les bonnes données.
   Ici, django a besoin de rassembler l'ensemble des thèmes, il doit donc accéder à la base de données.

3. Django charge les fichiers dans `BASEDIR/webapp/qcmonge/models` qui contienent la description de
   la base de données.
   Ces fichiers font le lien entre django et la vraie base. D'un coté, il interprète la demande de django en terme
   de SQL pour interroger la base; de l'autre, il interprète la réponse de la base pour la transformer
   en objets python.
   Ceci est bien pratique car on n'a pas à apprendre le SQL... Django s'en occupe.

4. Retour dans la vue.
   Django a les données, il lui faut maintenant les mettre en forme et les envoyer à l'utilisateur.
   Pour cela, il faut utiliser le template `qcmonge/voir_themes.html` auquel on passe les paramètres de la requête et
   le contexte qu'on vient de créer.

5. Django va donc voir le template. Il se trouve dans le répertoire `BASEDIR/webapp/qcmonge/templates/qcmonge/`.
   Ce fichier est un mélange de html et de balises spéciales qui sont toutes de la forme `{{ ... }}` ou `{% ... %}`.
   Si c'était du LaTeX, les premières seraient des commandes, les secondes des environnements.
   La première balise nous dit que nous étendons le template `base.html`.

6. Allons voir ce template : `BASEDIR/webapp/qcmonge/templates/base.html`. On reconnaît une page html plus classique.
   Il y a la présence de balises `block`. En fait, django va envoyer cette page en remplaçant tous les blocs
   qui apparaissent dans `voir_themes.html`. Ceci fait que l'ensemble des pages est homogène.
   Si on veut ajouter un lien dans un menu, il suffit de l'ajouter dans le fichier de base pour
   qu'il se retrouve sur toutes les pages.

7. Django envoie cette page et l'utilisateur est content.
   Il se trouve que cette page nécessite un fichier CSS pour être affichée correctement.
   Celui-ci, ainsi que tous les fichiers statiques (CSS, javascript,...)
   se trouve dans les répertoires `BASEDIR/webapp/qcmonge/static/css/` et `BASEDIR/webapp/qcmonge/static/js/`.
   De même, quand django a besoin de l'image d'une question, il va la chercher dans `BASEDIR/data/enonces`.


Voilà, en gros, comment ça se passe.

Les requêtes ajax se traitent comme les autres (d'ailleurs django ne fait pas la différence).
Pour rappel, un requête ajax est un requête faite par javascript (ou autre) qui n'affiche pas une nouvelle page
mais vient compléter la page courante. Un exemple est tout ce qui se passe autour de la construction de QCM :
à aucun moment la page n'est rafraîchie.

Beaucoup de choses, la-dedans, sont modifiables et je n'ai fait qu'effleurer les choses.
J'espère juste que ça donne un petit tour d'horizon de la logique du bouzin. Pour résumer :

* La base de données se situe dans `BASEDIR/webapp/db.sqlite3`
* Les modèles, c'est-à-dire le lien entre python et la base de données sont dans `BASEDIR/webapp/qcmonge/models/`
* Les vues, c'est-à-dire le traitement des données sont dans `BASEDIR/webapp/qcmonge/views/`
* Les templates, c'est-à-dire les pages html prêtes à être utilisées sont dans `BASEDIR/webapp/qcmonge/templates/`
* Les fichiers CSS sont dans `BASEDIR/webapp/qcmonge/static/css/`
* Les fichiers javascript qui s'occupent notamment de l'ajax sont dans `BASEDIR/webapp/qcmonge/static/js/`
* Les images génériques, c'est-à-dire non liées aux données sont dans `BASEDIR/webapp/qcmonge/static/images/`
* Les images liées à la base de données (les exercices) sont dans `BASEDIR/data/`
