
**/!\ Cette documentation n'est pas à jour /!\**


Formulaires
===========

On décrit ici les classes de formulaires.
Elles permettent de faire les premières vérifications.


ThematiqueForm
--------------

Dans le fichier `thematique.py`.

C'est lui qui gère le triplet Matière/Théme/sous-Thème coté serveur.

Il comporte:
 * `matiere`: une liste déroulante comportant l'ensemble des matières disponibles.
 * `theme`: une liste déroulante pour choisir le thème.
 * `soustheme`: une liste déroulante pour choisir le sous-thème.

Lors de la validation, il s'assure que le sous-thème fait bien partie du Thème, qui lui-même est bien dans la Matière.





ImportTexForm
-------------

Dans le fichier `importTex.py`.

Ce formulaire permet de téléverser un fichier TeX et de l'analyser.

Il hérite de `ThematiqueForm`.

Outre les champs de `ThematiqueForm`, il comporte:
 * `texupload`: un champ pour sélectionner le fichier à télécharger.
 
Lors de la validation, il s'assure que le fichier a le bon mime/type et tente de deviner l'encoding pour le basculer en UTF-8.
 




QuestionImportForm
------------------

Dans le fichier `importQuestion.py`.

Ce formulaire permet à l'utilisateur de valider une question.

Il hérite de `ThematiqueForm`.

Outre les champs de `ThematiqueForm`, il comporte:
 * `qtype`: une liste déroulante pour choisir le type de question (simple ou multiple)
 * `keepme`: une case à cocher pour indiquer si on garde la question ou on la supprime.
 * `texheader`: une zone de texte pour modifier le préambule LaTex.
 * `texcontent`: une zone de texte pour modifier l'énoncé LaTeX.
  
Il comporte aussi un attribut `thematique`.

Lors de la validation du préambule et de l'énoncé, on supprime les caractères `\r` de retour à la ligne de windows. `\n` est bien suffisant.

Pour la validation globale, le le sous-thème est précisé, on stocke l'objet `SousTheme` correspondant dans `thematique`.
En cas de problème, on y stocke `None`.
