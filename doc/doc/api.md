api
===

Ces vues servent à envoyer des json en asynchrone. Elles n'envoient pas de html.

Toutes les fonctions reçoivent leurs données par un json dans le champ 'jsondata'.
Elles retournent toutes un json avec un champ `success` et un champ `reason` ou `data`: 

    { 'success': False, 'reason': "......" }
    { 'success': True , 'data': ...... }
    

Allons y pour le détail.

### api_structure ###

Méthode accéssible en GET ou POST.

Retourne la structure encadrant les données : matières, thèmes et sous-thèmes.
Les données se présentent sous la forme:

    { matiere_1: { theme_1_1 : [sous-theme_1_1_1, sous-theme_1_1_2,...],
                   theme_1_2 : [sous-theme_1_2_1, sous-theme_1_2_2,...],
                   ...
                 },
      matiere_2: ...
    }

**Erreur**: aucune a priori




### api_search_questions ###

Méthode uniquement accéssible en POST.

Retourne les questions correspondant à la recherche.

Les paramètres de recherche suivants sont obligatoires :
 + `matiere`: chaîne
 + `theme`: chaîne
 + `soustheme`: chaîne
 + `type`: chaine contenant les caractères 'S', 'M' ou 'O'. pour déterminer quel type de questions choisir (simple, multiple, ouverte)
Les paramètres suivants sont optionnels :
 + `qlength`: entier représentant le nombre de questions voulues (5 par défaut)
 + `qoffset`: entier représentant l'offset des questions (0 par défaut)
 
Parmi toutes les questions filtrées, on envoie celles entre `qoffset` et `qoffset + qlength`.

Les données renvoyées sont:

    { "offset": offset,
      "total": length,
      "questions": [ question_1, question_2, ... ]
    }

L'`offset` est l'offset réel. Il peut être différent de celui demandé s'il n'y a pas assez de questions.
Le `total` est le nombre de questions qui correspondent aux critères.
    
Les questions sont des dictionnaires contenant les clés:
 + `id`: entier représentant l'id de la question
 + `enonce`: le texte LaTeX de la question, en attendant le chargement de l'image (ou si elle n'existe pas)
 + `image`: l'url de l'image de la question.

**Erreur**: s'il manque un paramètre de recherche


### api_infos_questions ###

Méthode uniquement accéssible en POST.

Renvoie les informations des questions demandées.

En entrée, on trouve un tableau d'identifiants de question.
En sortie, on trouve un tableau de dictionnaires:

    [ { "id": id, "enonce": "...", "image": url }, ... ]
    
**Erreur**: si une des questions n'est pas présente dans la base.
