
Les vues
========


general.py
----------

Ce fichier s'occupe des pages générales, c'est-à-dire de l'acceuil et des pages de statistiques.


### index ###

*Page provisoire.*

C'est censé être la page d'accueil. Elle est pour l'instant très rudimentaire.
Elle permet de voir les utilisateurs inscrits et quelques liens.



### voir_user ###

*Page provisoire.*

Page de profile d'un utilisateur, regroupant quelques informations.
Elle a besoin d'un `user_login` valide.



### voir_themes ###

*Page provisoire.*

Affiche la liste des matières, thèmes et sous-thèmes.
L'affichage est hierarchique.
Pour chaque sous-thème, on affiche le nombre de questions en base.





exportation.py
--------------

Ces vues gèrent la recherche de questions et le download du TeX.


### export_qcm ###

Page accéssible uniquement en GET.

Renvoie une page de recherche sommaire.
La structure des données est téléchargé par l'api.
Les recherches se font via l'api json.
On précharge seulement la matière préférée de l'utilisateur.

On peut chercher en sélectionnant la matière, le thème, le sous-thème.
On peut sélectionner le type de question (simple, multiple ou ouverte).

Le javascript se charge de la gestion asynchrone: chargement des questions,
navigation dans les questions sélectionnées ou pas.

Ensuite, on affiche un récapitulatif avec la sélection uniquement dans lequel
on peut organiser les groupes de questions pour organiser les mélanges dans le questionnaire.
Ceci se fait par drag-n-drop.

Enfin, on affiche un bouton pour télécharger le zip des questions.



### get_qcm_content ###

Méthode uniquement accéssible en POST.

Retourne un fichier zip contenant toutes les informations nécessaires à la compilation
des questions données en paramètre:

    { "nom_groupe1": [id1, id2,...],
      "nom_groupe2": [id3,...],
      ...
    }
    
Les clés sont les noms des groupes de questions.
Une clé vide `""` signifie des questions sans groupe.

Le fichier zip contient au minimum les fichiers `header.tex` (préambule) et `content.tex` (les questions).
Il contient aussi toutes les images et autres fichiers nécessaires.

**Erreur**: s'il y a une question qui n'est pas dans la base.




importation.py
--------------

Ici on gère l'importation de questions dans la base.


### import_qcm ###

Pour utilisateur autorisé uniquement.

Différents comportements si GET ou POST.

En mode GET:
On envoie le formulaire d'importation et un `ImportTexForm` vide.

En mode POST:
On récupère le fichier du formulaire `ImportTexForm`.
On extrait les questions et on tente de les compiler.
Si tout va bien, on stocke les données en base,
et on redirige vers la deuxième phase de l'importation : `finalise_import_qcm`.
Si ça se passe mal, on reste ici et on renvoie le formulaire en l'état.



### finalise_import_qcm ###

Pour utilisateur autorisé uniquement.

Différents comportements si GET ou POST.

En mode GET:
On crée autant de `QuestionImportForm` qu'il y a de questions non validées de l'utilisateur.
On envoie la page de finalisation avec tous les formulaires, leurs images et leurs enoncés.
L'utilisateur a la possibilité de modifier les thèmes, préambule LaTeX et enoncé LaTeX.

En mode POST:
On valide un par un les formulaires des questions.
Si la question n'a pas changé (préambule ou énoncé) et que le thème est valide, on enregistre la question définitivement.
Sinon, on renvoie la question.
On peut aussi choisir de supprimer la question (qui sera supprémiée en base aussi).
Si plus de question à valider, on redirige vers la visualisation des thèmes.

