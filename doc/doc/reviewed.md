
Etat des fichiers à relire
==========================

 * {+ OK: admin.py +}

 * {+ OK: urls.py +}


 * {+ OK: models/classement.py +}

 * {+ OK: models/dependances.py +}

 * {+ OK: models/general.py +}

 * {+ OK: models/question.py +}


GENERAL
-------

 * {- views/general.py -}

 * {+ OK: templates/base.html +}

 * {- templates/qcmonge/index.html -}

 * {- templates/qcmonge/login.html -}

 * {- templates/qcmonge/voir_themes.html -}

 * {- static/css/qcmonge.css -}



EXPORTATION
-----------

 * {+ OK: views/api.py +}

 * {+ OK: views/exportation.py +}

 * {+ OK: static/js/export.js +}

 * {+ OK: templates/qcmonge/export_qcm.html +}



IMPORTATION
-----------

 * {+ OK: views/importation.py +}

 * {+ OK: static/js/base-imex.js +}

 * {+ OK: static/js/importation.js +}

 * {+ OK: static/js/final-import.js +}

 * {+ OK: templates/qcmonge/import1_qcm.html +}

 * {+ OK: templates/qcmonge/import2_qcm.html +}

 * {+ OK: forms/importQuestion.py +}

 * {+ OK: forms/importTex.py +}

 * {- forms/thematique.py -}
 
