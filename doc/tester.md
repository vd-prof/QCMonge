# pour tester le projet

Cette procédure permet de tester le projet.
Je suppose que vous avez installé toutes les dépendances (python3, django, LaTeX,...).
À partir de là, je pars de zéro pour arriver à quelque chose de testable.

** Attention ** Il ne faut en aucun cas appliquer cette procédure pour mettre l'application en production !


Je vous conseille de lire l'ensemble des instructions avant de commencer.
Ça permet d'avoir une vision d'ensemble de ce qu'il faudra faire avant de commencer.



### Télécharger le projet

1. Ouvrez un terminal puis déplacez vous dans le répertoire de votre choix

2. `git clone https://framagit.org/vd-prof/QCMonge.git` permet de créer le répertoire `QCMonge` et copier l'état actuel du projet à partir du dépot framagit.

3. Déplacer vous `cd QCMonge/qcmonge/webapp`.  Si vous voulez tester une branche différente de la branche principale, c'est le moment de le dire `git checkout nom-de-la-branche`. Si vous n'avez pas compris la dernière phrase, oubliez-la, ce n'est pas important !


### Tester l'application WEB

1. Créez la base de données :
	*  `python3 manage.py makemigrations qcmonge`
	*  `python3 manage.py migrate`

2. Il vous faudra créer un super-utilisateur pour pouvoir toucher à la base : `python3 manage.py createsuperuser` (l'adresse mail ne sert à rien).

3. Lancez le serveur web de test interne à django : `python3 manage.py runserver`. Attention, ce serveur n'est pas sécurisé. Il ne doit pas être utilisé en production.

4. Ouvrez votre navigateur favori et entrez l'url de test du projet : <http://localhost:8000/qcmonge>



### Pour commencer à jouer

1. Commencez par aller dans la partie administration : <http://localhost:8000/admin> et identifiez-vous avec les identifiants super-utilisateur que vous venez de créer.
  Créer (au moins) une matière, un thème et un sous-thème.

2. Créez ensuite un utilisateur. Il n'est en effet pas recommandé d'utiliser l'administrateur «au quotidien», y compris pour tester.
  Il faudra en particulier rentrer sa matière préférée. Comme pour l'administrateur, le mail n'est pas important.
  Donnez-lui les tous les droits associés à `qcmonge` : création, modification, effacement.
  Ceci lui permettra d'importer des questions.

3. Vous êtes paré pour tester l'appli. Rendez-vous sur  <http://localhost:8000/qcmonge>.
  Déconnectez vous et reconnectez vous avec le nouvel utilisateur.




### Le mot de la fin

Testez comme vous voulez, vous ne pouvez rien casser. Au pire, votre copie du projet ne fonctionne plus.
Il suffira alors de recommencer les étapes ci-dessus pour avoir une copie toute fraîche...

Si vous voulez voir les requêtes réseau qui sont faîtes, le terminal où vous avez lancé le serveur vous les montre.

Si vous voulez encore plus de détails, appuyez sur `F12` dans `firefox` : vous verrez apparaître la console de
   debug. L'onglet réseau vous fera voir les requêtes, leur durée, leurs paramètres,...

N'hésitez pas à rapporter le moindre bug, même minime ainsi que toutes les améliorations que vous souhaiteriez voir.
Pour cela, utilisez les tickets dans framagit.
Nous ne garantissons pas de traiter les bugs rapidement mais seulement dans un temps raisonnable.
Nous ne garantissons pas non plus d'implémenter votre amélioration, mais nous y jetterons au moins un coup d'oeil.

Si vous voulez nous aider, n'hésitez pas à vous faire connaître.

