# Ce qui est nécessaire

Voici ma configuration. Ceci permet de faire tourner QCMonge et/ou l'améliorer.

* GNU/Linux. On doit pouvoir faire avec autre chose mais je ne peux pas tester.

* python >= 3.4

* django >= 2.0

* modules python additionnels : json, html, datetime, chardet et  magic.
  Normalement, ils sont tous dans la distribution standard sauf les deux derniers.
  **Attention**, les modules doivent être compatibles avec votre version de python.
  **Attention**, il existe au moins deux versions du paquet magic.
  Celle qui nous intéresse est celle distribuée par debian (nom du paquet debian: python3-magic).
  Il existe une autre version distribuée par pip. Elles sont incompatibles !

* LaTeX avec toutes les dépendances dont vous avez besoin.

* imagemagic pour la conversion des pdf en png.
  **Attention**, certaines versions de imagemagic n'autorise pas la conversion.
  Il faut ajouter la ligne `<policy domain="coder" rights="read | write" pattern="PDF" />`
  juste avant `</policymap>` dans le fichier `/etc/ImageMagick-7/policy.xml` pour résoudre le problème.

* git pour obtenir les dernières versions du projet.
