QCMonge
=======

QCMonge est un gestionnaire de base de données pour exercices de Auto Multiple Choice (ou AMC pour les intimes).

D'après le site d'[AMC](https://www.auto-multiple-choice.net) :
> Auto Multiple Choice est un ensemble d'utilitaires permettant de créer, gérer et corriger automatiquement des questionnaires à choix multiples (QCM).

QCMonge a pour ambition de mutualiser les questions à travers une interface web agréable et claire.

L'utilisateur a la possibilité de choisir ses questions dans la base de données en fonction plusieurs critères.
Une fois ses questions sélectionnées, il ne lui reste plus qu'à télécharger son questionnnaire et l'importer dans AMC.

Les utilisateurs habilités ont aussi la possibilités d'alimenter la base de données avec leurs propres QCM rédigés avec LaTeX.

Il s'agit pour l'instant d'une version alpha.

### Pourquoi QCMonge ?

Parce que le logiciel gère des QCM et que les initiateurs du projet (VD, CB et J-PB) travaillent au lycée Monge de Chambéry...

### Qu'y a-t-il sous le capot ?

Nous travaillons avec _python_, plus précisement [Django](https://www.djangoproject.com) pour l'interface web.
La base de données est sous _sqlite_.
Il y a aussi du _javascript_ pour les requêtes asynchrones (ajax), et bien sûr du _HTML_ et _CSS_ pour la partie web.

### Ce que sait faire QCMonge pour l'instant

 * Gestion des utilisateurs ansi que leurs rôles dans l'application (merci django).
 * Recherche dans la base par :
   * matière, thème et sous-thème
   * type de question (simple ou multiple)
 * Sélection des questions (en ajax)
 * Téléchargement des questions sélectionnées dans un fichier zip contenant les questions TeX et un préambule.
 * Importation de QCM avec analyse des questions pour déterminer leur encodage (pasage en utf8), leur type (en cours de relecture)
 * Compilation LaTeX pour générer les images des questions.



### Ce que nous aimerions que QCMonge fasse

 * Fournir un fichier TeX prêt à compiler avec
   * gestion des fichiers annexes (images, code, import,...)
   * possibilité de paramétrer finement le fichier TeX final : plusieurs colonnes, plusieurs groupes de questions,
 présentation,...
 * Détection de questions redondantes dans la base.
 * Interface web plus conviviale (du vrai CSS, quoi...).


### plus de rensignements

 Allez voir dans le dossier `doc`. Il y a des fichiers pour :
 * Les ingrédients nécessaires pour faire tourner le projet.
 * La marche à suivre pour tester le projet.
 * Le principe de fonctionnement de django et du projet.

